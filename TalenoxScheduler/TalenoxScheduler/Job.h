//
//  Job.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Job : NSObject

@property (nonatomic) NSInteger jobID;
@property (nonatomic) double minSalary;
@property (nonatomic) double maxSalary;
@property (nonatomic) double salary;
@property (nonatomic) NSString * shiftName;
@property (nonatomic) NSString *salaryType;
@property (nonatomic) NSString *ownerName;
@property (nonatomic) NSDate * startTime;
@property (nonatomic) NSDate * endTime;
@property (nonatomic) NSString *location;
@property (nonatomic) NSString *jobDescription;
@property (nonatomic) NSString *deadline;
@property (nonatomic) NSString *workingTimeInfo;

@end