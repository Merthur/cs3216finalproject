//
//  PersonalProfile.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 15/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "PersonalProfile.h"

@implementation PersonalProfile
-(id) initWithPicture:(NSString *)picutre andName:(NSString *)name{
    self = [super init];
    if (self) {
        self.profilePicture = picutre;
        self.userName = name;
    }
    return self;
}

@end
