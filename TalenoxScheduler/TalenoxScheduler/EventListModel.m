//
//  EventModel.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-19.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import "EventListModel.h"

static BOOL loaded = NO;

@interface EventListModel()

@property (nonatomic) NSMutableArray *eventList;
@property (nonatomic) NSMutableDictionary *response;

@end


@implementation EventListModel

+ (EventListModel*)sharedInstance {
    static EventListModel *sharedInstance;
    
    if (!loaded) {
        sharedInstance = [[self alloc] init];
    }
    
    return sharedInstance;
}

- (EventListModel*) init {
    self = [super init];
    
    if (self) {
        [self clearData];
        if ([[UserInfoModel sharedInstance] getUserEmployeeId]) {
            [self getAvailabilitiesFromServer];
        }
    }
    
    return self;
}

- (void) withLoadingDelegate:(id<loadingDelegate>)delegate {
    if (loaded) {
        [delegate nextViewController];
    } else {
        self.loadingDelegate = delegate;
        loaded = YES;
    }
}

- (void) getAvailabilitiesFromServer {
    NSString *employeeId = [[UserInfoModel sharedInstance] getUserEmployeeId];
    
    HttpClient *client = [HttpClient sharedInstance];
    [client getAvailabilityWithEmployeeID:employeeId
                                  success:^(NSURLSessionDataTask *task, id responseObject){
                                      self.response = responseObject;
                                      [self createEventListFromResponse];
                                  }
                                  failure:^(NSURLSessionDataTask *task, NSError *error) {
                                      NSLog(@"%@", error);
                                      [self.loadingDelegate nextViewController];
                                  }];
}

- (void) createEventListFromResponse {
    NSArray *array = [self.response objectForKey:@"availabilities"];
    
    for (int i=0; i<[array count]; i++) {
        NSArray *list = [[array objectAtIndex:i] objectForKey:@"availabilities"];
        
        for (NSDictionary *dict in list) {
            MAEvent *event = [[MAEvent alloc] init];
            event.backgroundColor = [UIColor colorWithRed:225/255.0 green:236/255.0 blue:187/255.0 alpha:0.6];
            event.textColor = [UIColor whiteColor];
            event.allDay = NO;
            
            NSDate *start = [NSDate dateWithTimeIntervalSince1970:[[dict objectForKey:@"start_time"] doubleValue]];
            NSDate *end = [NSDate dateWithTimeIntervalSince1970:[[dict objectForKey:@"end_time"] doubleValue]];
            event.start = start;
            event.end = end;
            event.event_id = [[dict objectForKeyedSubscript:@"id"] integerValue];
            
            [self.eventList addObject:event];
        }
    }
    
    [self.loadingDelegate nextViewController];
}

- (NSArray*) getEventList {
    return self.eventList;
}

- (void) addNewEvent:(MAEvent*) event withDelegate:(id<SaveEventDelegate>)delegate{
    [self.eventList addObject:event];
    NSUInteger index = [self.eventList indexOfObject:event];
    [self createEvents:index withDelegate:delegate];
}

- (void) createEvents:(NSUInteger) index withDelegate:(id<SaveEventDelegate>)delegate{
    [[HttpClient sharedInstance] createEventWithSlot:[self.eventList objectAtIndex:index]
                                             success:^(NSURLSessionDataTask *task, id responseObject) {
                                                 NSLog(@"%@", responseObject);
                                                 MAEvent *event = [self.eventList objectAtIndex:index];
                                                 NSUInteger eventID = [[responseObject objectForKey:@"id"] integerValue];
                                                 event.event_id = eventID;
                                                 [delegate databaseUpdate:0 andResult:1];
                                             }
                                             failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                 NSLog(@"%@", error);
                                                 [delegate databaseUpdate:0 andResult:0];
                                             }];
}

- (void) updateEventAtIndex:(NSInteger)eventID
               withNewEvent:(MAEvent*)event
               withDelegate:(id<SaveEventDelegate>)delegate{
    
    int index = -1;
    
    for (int i=0; i<self.eventList.count; i++) {
        MAEvent *item = [self.eventList objectAtIndex:i];
        if (item.event_id == eventID) {
            index = i;
            break;
        }
    }
    
    if (index > -1) {
        [[HttpClient sharedInstance] updateEventWithSlot:event
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     [self.eventList replaceObjectAtIndex:index
                                                                               withObject:event];
                                                     [delegate databaseUpdate:1 andResult:1];
                                                 }
                                                 failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     NSLog(@"%@", error);
                                                     [delegate databaseUpdate:1 andResult:0];
                                                 }];
    }
}

- (void) deleteEvent:(MAEvent *)event withDelegate:(id<SaveEventDelegate>)delegate{
    [[HttpClient sharedInstance] deleteEventWithSlot:event
                                             success:^(NSURLSessionDataTask *task, id responseObject) {
                                                 NSLog(@"%@", responseObject);
                                                 [self.eventList removeObject:event];
                                                 [delegate databaseUpdate:2 andResult:1];
                                             }
                                             failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                 NSLog(@"%@", error);
                                                 [delegate databaseUpdate:2 andResult:0];
                                             }];
}

- (void) clearData {
    self.eventList = [[NSMutableArray alloc] init];
    loaded = NO;
}

@end
