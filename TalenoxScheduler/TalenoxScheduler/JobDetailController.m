//
//  JobDetailController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "JobDetailController.h"
#import "JobListController.h"

@interface JobDetailController ()

@end

@implementation JobDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.jobTitle setText:@"Job Detail"];
    [self.titleLabel setText:self.shiftName];
    [self.durationLabel setText:@"2014-11-14 14:00 - 17:00"];
    [self.locationLabel setText:self.location];
    [self.jobDescriptionLabel setText:self.jobDescription];
    [self.applyButton setTitle:self.buttonMessage forState:UIControlStateNormal];
    if ([self.buttonMessage isEqualToString:@"Applied"]) {
        [self.applyButton setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
        [self.applyButton setUserInteractionEnabled:NO];
    } else if ([self.buttonMessage isEqualToString:@"Withdraw"]) {
        [self.applyButton setBackgroundColor:[UIColor colorWithRed:230/255.0 green:201/255.0 blue:129/255.0 alpha:1]];
    }
    [self.applyButton.layer setCornerRadius:5.0];
    [self.applyButton setClipsToBounds:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)applyAJob:(id)sender {
    [self.view makeToastActivity];
    if ([self.buttonMessage isEqualToString:@"Apply"]) {
        [[HttpClient sharedInstance]
         applyJobWithJobID: self.jobID
         success:^(NSURLSessionDataTask *task, id responseObject) {
             [self jobAppliedSuccessfulWithResponse:responseObject];
             [self.applyButton setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
         } failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"%@", error);
             [self jobAppliedFail];
         }];
    } else if ([self.buttonMessage isEqualToString:@"Withdraw"]) {
        [[HttpClient sharedInstance]
         withdrawJobWithAppliedID:self.appliedID
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"%@", responseObject);
             [self jobWithdrawSuccessfulWithResponse:responseObject];
             [self.applyButton setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"%@", error);
             [self jobWithdrawFail];
         }];
    }
}

- (void) jobAppliedSuccessfulWithResponse:(id)response{
    [self.view hideToastActivity];
    
    [[JobList sharedInstance] appliedJob: self.jobID
                           withAppliedID: [[response objectForKey:@"id"] integerValue]];
    
    [self.view makeToast: APPLY_JOB_SUCCESS duration:3 position:@"center"];
    [self.applyButton setTitle:@"Applied" forState:UIControlStateNormal];
    [self.applyButton setUserInteractionEnabled:NO];
}

- (void) jobAppliedFail {
    [self.view hideToastActivity];
    [self.view makeToast: APPLY_JOB_FAIL duration:3 position:@"center"];
}

- (void) jobWithdrawSuccessfulWithResponse:(id)response{
    [self.view hideToastActivity];
    [self.view makeToast: WITHDRAW_JOB_SUCCESS duration:3 position:@"center"];
    
    if ([[response objectForKey:KEY_STATUS] isEqualToString:@"1"]) {
        [[JobList sharedInstance] withdrawJobWithAppliedID:self.appliedID];
    }
    
    [self.applyButton setTitle:@"Withdrawed" forState:UIControlStateNormal];
    [self.applyButton setUserInteractionEnabled:NO];
}

- (void) jobWithdrawFail {
    [self.view hideToastActivity];
    [self.view makeToast: WITHDRAW_JOB_FAIL duration:3 position:@"center"];
}

- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
