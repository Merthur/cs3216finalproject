//
//  JobApplied.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-12.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobApplied : NSObject

@property (nonatomic) NSInteger appliedID;
@property (nonatomic) NSInteger jobID;
@property (nonatomic) NSInteger userID;
@property (nonatomic) NSInteger status;

@end
