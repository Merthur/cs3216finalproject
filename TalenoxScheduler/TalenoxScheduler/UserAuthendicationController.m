//
//  UserAuthendicationController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 11/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "UserAuthendicationController.h"

@interface UserAuthendicationController ()

@end

@implementation UserAuthendicationController {
//    NSTimer *animationTimer;
    BOOL keyboardShow;
}

#pragma mark - view load
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    keyboardShow = NO;
    [self.userPwd setSecureTextEntry:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self checkForLoginInfo];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    CALayer *btnLayer = [self.loginBtn layer];
    [btnLayer setMasksToBounds:YES];
    [btnLayer setCornerRadius:10.0f];
    
    [UIView animateWithDuration:20
                     animations:^{
                         NSLog(@"come in animation");
                         self.BGimage.frame = CGRectMake(self.BGimage.frame.origin.x,
                                                         self.BGimage.frame.origin.y,
                                                         self.BGimage.frame.size.width*0.8,
                                                         self.BGimage.frame.size.height*0.8);
                     }];
}

- (void) viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification object:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - user login
- (IBAction)fetchUserInfo:(id) sender{
    if ([[self.userName text] isEqualToString:@""] || [[self.userPwd text] isEqualToString:@""]) {
        [self.view makeToast:@"Please enter email and password"
                    duration:3.0
                    position:@"center"];
    } else {
        [self userCanLogin];
    }
}

- (void) userCanLogin {
    [self.view makeToastActivity];
    [self.userName resignFirstResponder];
    [self.userPwd resignFirstResponder];
    HttpClient *client = [HttpClient sharedInstance];
    
    [client userLogin:[self.userName text]
             Password:[self.userPwd text]
              success:^(NSURLSessionDataTask *task, id responseObject) {
                  if ([responseObject objectForKey:@"errors"]) {
                      [self.view hideToastActivity];
                      NSLog(@"Login Fail");
                      [self.userPwd setText:@""];
                      [self clearLoginInfo];
                      [self.view makeToast:@"Wrong email or password"
                                  duration:3.0
                                  position:@"center"];
                  } else {
                      UserInfoModel *user = [UserInfoModel sharedInstance];
                      user.loadingDelegate = self;
                      [user userLogin:responseObject];
                  }
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  NSLog(@"Error: %@",error);
                  [self.view hideToastActivity];
                  [self.view makeToast:@"Cannot Login. Try again later."
                              duration:3.0
                              position:@"center"];
              }];
}

- (void) nextViewController{
    LoginInfo *login = [[LoginInfo alloc] initWithEmail:[self.userName text] andPassword:[self.userPwd text]];
    [self saveLoginInfo:login];
    
    [self.view hideToastActivity];
    UIViewController *dashboardView = [self.storyboard instantiateViewControllerWithIdentifier:DASHBOARD_CONTROLLER];
    [self.navigationController presentViewController:dashboardView animated:YES completion:nil];
}

#pragma mark - login info
- (void) checkForLoginInfo {
    LoginInfo *login = [self loadLoginInfo];
    
    if (login) {
        [self.userName setText:[login getEmail]];
        [self.userPwd setText:[login getPassword]];
    }
}

- (void) saveLoginInfo: (LoginInfo *) loginInfo {
    NSMutableData *data = [[NSMutableData alloc] init];
    
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData: data];
    
    [archiver encodeObject:loginInfo forKey:KEY_LOGIN_INFO];
    [archiver finishEncoding];
    
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent: FILE_LOGIN_INFO];
    BOOL success = [data writeToFile: filePath atomically: YES];
    
    if (!success) {
        NSLog(@"save user login info fail");
    }
}

- (LoginInfo*) loadLoginInfo {
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:FILE_LOGIN_INFO];
    NSData *data = [[NSData alloc] initWithContentsOfFile: filePath];
    LoginInfo *login = [[LoginInfo alloc] init];
    
    if (data) {
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData: data];
        login = [unarchiver decodeObjectForKey:KEY_LOGIN_INFO];
        [unarchiver finishDecoding];
    } else {
        login = nil;
    }
    
    return login;
}

- (void) clearLoginInfo {
    LoginInfo *login = [[LoginInfo alloc] initWithEmail:@"" andPassword:@""];
    [self saveLoginInfo:login];
}

#pragma mark - keyboard show
// Setup keyboard handlers to slide the view containing the table view and
// text field upwards when the keyboard shows, and downwards when it hides.
- (void)keyboardWillShow:(NSNotification*)notification
{
    if (!keyboardShow) {
        keyboardShow = YES;
        [self moveView:[notification userInfo] up:YES];
    }
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    if (keyboardShow) {
        keyboardShow = NO;
        [self moveView:[notification userInfo] up:NO];
    }
}

- (void)moveView:(NSDictionary*)userInfo up:(BOOL)up
{
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]
     getValue:&keyboardEndFrame];
    
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]
     getValue:&animationCurve];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey]
     getValue:&animationDuration];
    
    // Get the correct keyboard size to we slide the right amount.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    int y = keyboardFrame.size.height * (up ? -1 : 1);
    self.view.frame = CGRectOffset(self.view.frame, 0, y);
    
    [UIView commitAnimations];
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    if ([self.userName isFirstResponder]) {
        [self.userName resignFirstResponder];
    }else if ([self.userPwd isFirstResponder]){
        [self.userPwd resignFirstResponder];
    }
}

@end
