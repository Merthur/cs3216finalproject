//
//  HttpClient.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-11.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import "HttpClient.h"

@implementation HttpClient

static UserInfoModel *currentUser;

+ (HttpClient *)sharedInstance{
    currentUser = [UserInfoModel sharedInstance];
    
    static dispatch_once_t once;
    static HttpClient *sharedInstance;
    dispatch_once(&once, ^ { sharedInstance = [[self alloc] init]; });
    return sharedInstance;
}

- (id)init{
    self = [super init];
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.requestSerializer setValue:ACCEPT forHTTPHeaderField:ACCEPT_VALUE];
        [self.requestSerializer setValue:CONTENT_TYPE_VALUE forHTTPHeaderField:CONTENT_TYPE];
        [self.requestSerializer setValue:HOST_VALUE forHTTPHeaderField:HOST];
        [self.requestSerializer setValue:RUNSCOPE_VALUE forHTTPHeaderField:RUNSCOPE];
        [self.requestSerializer setValue:USER_AGENT_VALUE forHTTPHeaderField:USER_AGENT];
    }
    
    return self;
}

#pragma mark - Authentication
- (void)userLogin:(NSString*)email
         Password:(NSString*) password
          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    NSDictionary *data = @{EMAIL:email,
                           PASSWORD:password
                           };
    
    [self POST:[REQUEST_DOMAIN stringByAppendingString:LOGIN_REQUEST]
    parameters:data
       success:^(NSURLSessionDataTask *task, id responseObject) {
           NSLog(@"Login Success");
           success(task, responseObject);
       }
       failure:^(NSURLSessionDataTask *task, NSError *error) {
           NSLog(@"Login Fail");
           failure(task, error);
    }];
}

#pragma mark - Get Attribute
- (void) getUserCompanyWithToken:(NSString*)token
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString *email = [[currentUser getUserInformation] objectForKey:EMAIL];
    [self.requestSerializer setAuthorizationHeaderFieldWithUsername:email password:token];
    
    [self GET:[REQUEST_DOMAIN stringByAppendingString:GET_COMPANY_REQUEST]
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          NSLog(@"get company information");
          success(task, responseObject);
      }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"cannot get company information");
          failure(task, error);
      }];
}

- (void) getEmployeeCacheKeyWithToken:(NSString*)token
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSString *email = [[currentUser getUserInformation] objectForKey:EMAIL];
    [self.requestSerializer setAuthorizationHeaderFieldWithUsername:email password:token];
    
    NSString *domain = [REQUEST_DOMAIN stringByAppendingString:GET_EMPLOYEE_REQUEST];
    domain = [domain stringByAppendingString:GET_EMPLOYEE_INIT];
    
    [self POST:domain
    parameters:nil
       success:^(NSURLSessionDataTask *task, id responseObject) {
           NSLog(@"Get cache key Success");
           success(task, responseObject);
       }
       failure:^(NSURLSessionDataTask *task, NSError *error) {
           NSLog(@"Cannot get cache key");
           failure(task, error);
       }];}

- (void) getEmployeeListWithCacheKey:(NSString*)cacheKey
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString *email = [[currentUser getUserInformation] objectForKey:EMAIL];
    NSString *token = [currentUser getToken];
    [self.requestSerializer setAuthorizationHeaderFieldWithUsername:email password:token];
    
    NSString *domain = [REQUEST_DOMAIN stringByAppendingString:GET_EMPLOYEE_REQUEST];
    domain = [domain stringByAppendingString:[NSString stringWithFormat:@"?key=%@", cacheKey]];
    
    [self GET:domain
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          NSLog(@"get employee information");
          success(task, responseObject);
      }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"cannot get employee information");
          failure(task, error);
      }];
}

#pragma mark - Event Operations
- (void) createEventWithSlot:(MAEvent*) event
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString *email = [[currentUser getUserInformation] objectForKey:EMAIL];
    NSString *token = [currentUser getToken];
    [self.requestSerializer setAuthorizationHeaderFieldWithUsername:email password:token];
    
    NSString *start = [NSString stringWithFormat:@"%f", [event.start timeIntervalSince1970]];
    NSString *end = [NSString stringWithFormat:@"%f", [event.end timeIntervalSince1970]];
    
    NSDictionary *data = @{EMPLOYEE_ID: [currentUser getUserEmployeeId],
                           START_TIME: start,
                           END_TIME: end};
    
    [self POST:[REQUEST_DOMAIN stringByAppendingString:CREATE_EVENT]
    parameters:data
       success:^(NSURLSessionDataTask *task, id responseObject) {
           NSLog(@"Create Success");
           success(task, responseObject);
       }
       failure:^(NSURLSessionDataTask *task, NSError *error) {
           NSLog(@"Create Fail");
           failure(task, error);
       }];
}

- (void) updateEventWithSlot:(MAEvent*) event
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString *start = [NSString stringWithFormat:@"%f", [event.start timeIntervalSince1970]];
    NSString *end = [NSString stringWithFormat:@"%f", [event.end timeIntervalSince1970]];
    NSString *eventId = [NSString stringWithFormat:@"/%ld", (long)event.event_id];
    
    NSDictionary *data = @{EMPLOYEE_ID: [currentUser getUserEmployeeId],
                           START_TIME: start,
                           END_TIME: end};
    
    NSString *domain = [[REQUEST_DOMAIN stringByAppendingString:CREATE_EVENT] stringByAppendingString:eventId];
    
    [self PUT:domain
   parameters:data
      success:^(NSURLSessionDataTask *task, id responseObject) {
          NSLog(@"Update Success");
          success(task, responseObject);
      }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"Update Fail");
          failure(task, error);
      }];
    
}

- (void) deleteEventWithSlot:(MAEvent*) event
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString *eventId = [NSString stringWithFormat:@"/%ld", (long)event.event_id];
    
    [self DELETE:[[REQUEST_DOMAIN stringByAppendingString:CREATE_EVENT] stringByAppendingString:eventId]
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"delete Success");
             success(task, responseObject);
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"delete Fail");
             failure(task, error);
         }];
}

- (void) getAvailabilityWithEmployeeID:(NSString*) employID
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:(NSHourCalendarUnit
                                                             |NSMinuteCalendarUnit
                                                             |NSSecondCalendarUnit)
                                                   fromDate:[NSDate date]];
    [dateComponents setDay:1];
    [dateComponents setMonth:1];
    [dateComponents setYear:1970];
    
    NSDate *start = [calendar dateFromComponents:dateComponents];
    
    [dateComponents setDay:30];
    [dateComponents setMonth:12];
    [dateComponents setYear:2070];
    NSDate *end = [calendar dateFromComponents:dateComponents];
    
    NSString *domain = [NSString stringWithFormat:@"%@%@%@", REQUEST_DOMAIN, GET_EMPLOYEE_FREE_SLOT, employID];

    domain = [domain stringByAppendingString:[NSString stringWithFormat:@"?start_time=%f", [start timeIntervalSince1970]]];
    domain = [domain stringByAppendingString:[NSString stringWithFormat:@"&end_time=%f", [end timeIntervalSince1970]]];
    
    [self GET: domain
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          NSLog(@"get employ availabilities Success");
          success(task, responseObject);
      }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"get employ availabilities Fail");
          failure(task, error);
      }];
}

- (void) getJobListWithEmployeeID:(NSString*)employID
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [self GET:[REQUEST_DOMAIN stringByAppendingString:GET_JOB_LIST]
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          NSLog(@"get job list information");
          success(task, responseObject);
      }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"cannot get job list information");
          failure(task, error);
      }];
}

- (void) getApplicationHistoryWithEmployeeID:(NSString*)employID
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    [self GET:[REQUEST_DOMAIN stringByAppendingString:GET_APPLICATION_HISTORY]
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          NSLog(@"get application history");
          success(task, responseObject);
      }
      failure:^(NSURLSessionDataTask *task, NSError *error) {
          NSLog(@"cannot get application history");
          failure(task, error);
      }];
}

- (void) applyJobWithJobID:(NSInteger)jobID
                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSDictionary *data = @{JOB_ID: [NSString stringWithFormat:@"%ld", (long)jobID]};
    
    [self POST:[REQUEST_DOMAIN stringByAppendingString:GET_APPLICATION_HISTORY]
    parameters:data
       success:^(NSURLSessionDataTask *task, id responseObject) {
           NSLog(@"apply Success");
           success(task, responseObject);
       }
       failure:^(NSURLSessionDataTask *task, NSError *error) {
           NSLog(@"apply Fail");
           failure(task, error);
       }];
}

- (void) withdrawJobWithAppliedID:(NSInteger)appliedID
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    
    NSString *job = [NSString stringWithFormat:@"/%ld", (long)appliedID];
    NSString *domain = [[REQUEST_DOMAIN stringByAppendingString:GET_APPLICATION_HISTORY]
                        stringByAppendingString:job];
    
    [self DELETE:domain
      parameters:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             NSLog(@"withdraw Success");
             success(task, responseObject);
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             NSLog(@"withdraw Fail");
             failure(task, error);
         }];
}

- (void) clearData {
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
}

@end
