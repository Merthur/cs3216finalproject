//
//  EventDetailController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 22/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAEvent.h"
#import "DayViewExampleController.h"
#import "EventUpdateViewDelegate.h"
#import "SaveEventDelegate.h"
#import "UIView+Toast.h"
#import "EventListModel.h"
#import "Constants.h"


@interface EventDetailController : UIViewController <SaveEventDelegate>
@property (nonatomic) NSString *eventname;
@property (nonatomic) NSString *starttime;
@property (nonatomic) NSString *endtime;
@property (nonatomic) NSInteger command;
@property (weak, nonatomic) MAEvent *event;

@property (strong, nonatomic) IBOutlet UIDatePicker *startPicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *endPicker;
@property (strong, nonatomic) IBOutlet UITextField *startTimeInput;
@property (strong, nonatomic) IBOutlet UITextField *eventName;
@property (strong, nonatomic) IBOutlet UITextField *endTimeInput;

@property (weak) id<EventUpdateViewDelegate> delegate;
@property (nonatomic) BOOL deletedPressed;

@property (nonatomic) TSPopoverController *controller;

@property (strong, nonatomic) IBOutlet UIButton *updateButton;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

- (IBAction)Delete:(id)sender;
- (IBAction)update:(id)sender;
- (IBAction)back:(id)sender;
-(bool) checkCrash:(MAEvent *)event currentID:(NSInteger) ID;
-(id)init;
-(id)initWithEventName:(NSString*)name andStartTime:(NSString*)stime andEndTime:(NSString*)etime;
@end
