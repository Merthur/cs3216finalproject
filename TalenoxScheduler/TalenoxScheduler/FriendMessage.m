//
//  FriendMessage.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 10/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "FriendMessage.h"

@implementation FriendMessage

-(id)init{
    self = [super init];
    if(self){
        
    }
    return self;
}

-(id)initWithFriendName:(NSString *)fName andMessageTime:(NSString *)mTime andLastMessage:(NSString *)message{
    self = [super init];
    if(self){
        self.friendName = fName;
        self.messageTime = mTime;
        self.lastMessage = message;
    }
    return  self;
    
}

@end
