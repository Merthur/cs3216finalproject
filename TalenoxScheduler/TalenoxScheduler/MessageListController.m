//
//  MessageListController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 9/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "MessageListController.h"

@interface MessageListController ()

@property (nonatomic) NSString *selectedFriendName;
@property (nonatomic) NSMutableArray *friendSet;
@property (nonatomic) NSArray *employObjects;
@property (nonatomic) NSMutableArray * groupList;

@end

@implementation MessageListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Initialize array that will store chat messages.
    self.chat = [[NSMutableArray alloc] init];
    self.conversationList = [[NSMutableArray alloc]init];
    self.populatedList = [[NSMutableArray alloc]init];
    self.friendSet = [[NSMutableArray alloc] init];
    self.groupList = [[NSMutableArray alloc] init];
    
    self.employObjects = [[UserInfoModel sharedInstance] getEmployeesInformation];
    [self createEmployeeModels];
    self.companyName = [[[UserInfoModel sharedInstance] getCompanyInformation] objectForKey:KEY_NAME];
    
    // Initialize the list group
    self.hostURL =[[NSMutableString alloc]initWithString:kFirechatNS];
    [self.hostURL appendString:self.userName];
    [self.hostURL appendString:@"_FriendList"];
    
    // Initialize the root of our Firebase namespace.
    NSLog(@"%@", self.hostURL);
    self.rootFirebase = [[Firebase alloc] initWithUrl:self.hostURL];
    [self.conversationList addObject:
     [[[UserInfoModel sharedInstance] getCompanyInformation] objectForKey:KEY_NAME]];
    [self makePopulatedList];
    
    [self.view makeToastActivity];
    [self.rootFirebase observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
        // Add the chat message to the array.
        [self.conversationList addObject:[snapshot.value objectForKey:@"name"]];
        [self makePopulatedList];
    }];
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) createEmployeeModels {
    for (id item in self.employObjects) {
        NSString *firstName = [item objectForKey:@"first_name"];
        firstName = [firstName stringByAppendingString:@" "];
        NSString *lastName = [item objectForKey:@"last_name"];
        NSDictionary *imageObject = [[item objectForKey:@"profile_picture"] objectForKey:@"image"];
        NSString *imgUrl;
        if (![imageObject isEqual:[NSNull null]]) {
            imgUrl = [[[imageObject objectForKey:@"image_file"] objectForKey:@"image_file"] objectForKey:@"url"];
        } else {
            imgUrl = nil;
        }
        
        PersonalProfile *profile = [[PersonalProfile alloc]
                                    initWithPicture:imgUrl
                                    andName:[firstName stringByAppendingString:lastName]];
        
        [self.groupList addObject:profile];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) getMessageList{
    NSMutableArray * stringParser;
    NSString * messageTitle;
    for(int i = 0; i < [self.chat count];i++){
        
        messageTitle = [self.chat objectAtIndex:i];
        stringParser = [[NSMutableArray alloc] initWithArray:[messageTitle componentsSeparatedByString:@"|"]];
        
        if([stringParser count]<2){
            return;
        }
        else{
            NSString *name1 = [stringParser objectAtIndex:0];
            NSString *name2 = [stringParser objectAtIndex:1];
            if([name1 isEqualToString:self.userName]){
                [self.messageList addObject:name2];
            }
            else if([name2 isEqualToString:self.userName]){
                [self.messageList addObject:name1];
            }

        }
        
    }
    
}

-(void) printConList{
    for (int i=0; i<[self.conversationList count]; i++) {
        NSLog(@"%@", [self.conversationList objectAtIndex:i]);
    }
}

-(void) makePopulatedList{
    for (int i=0; i<[self.conversationList count]; i++) {
        NSString *name = [self.conversationList objectAtIndex:i];
        
        if (![self.friendSet containsObject:name]) {
            [self.populatedList addObject:[[FriendMessage alloc]
                                           initWithFriendName: [self.conversationList objectAtIndex:i]
                                           andMessageTime:@""
                                           andLastMessage:@""]];
            [self.friendSet addObject:name];
        }
    }

    for(int i = 0 ; i< [self.conversationList count]; i++){
        NSString * friendName = [self.conversationList objectAtIndex:i];

        //get lastest message
        NSComparisonResult result = [self.userName compare:friendName];
        NSString *append = [[NSString alloc] init];
        if (i != 0) {
            if (result > 0) {
                append = [self.userName stringByAppendingString:friendName];
            } else {
                append = [friendName stringByAppendingString:self.userName];
            }
        } else {
            append = friendName;
        }
        
        NSMutableString *connectString = [[NSMutableString alloc]initWithString:kFirechatNS];
        [connectString appendString:append];
        
        Firebase *tempFirebase = [[Firebase alloc]initWithUrl:connectString];
        
        [tempFirebase observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
            NSString *messageContent = @"";
            NSMutableString *messageTime = [[NSMutableString alloc]initWithString:@""];
           
            messageContent = [snapshot.value objectForKey:@"text"];
            
            NSDate * currentTime = [NSDate date];
            if ([snapshot.value objectForKey:@"time"]) {
                [messageTime appendString:[snapshot.value objectForKey:@"time"]];
            }
            
            NSMutableString * presentTime = [[NSMutableString alloc]initWithString:@""];
            
            if(![messageTime isEqual: [NSNull null]]){
                NSString *dateString = messageTime;
                NSArray *timeStamp = [messageTime componentsSeparatedByString:@" "];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                // this is imporant - we set our input date format to match our input string
                // if format doesn't match you'll get nil from your string, so be careful
                [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
                
                NSDate *messageGetTime = [dateFormatter dateFromString:dateString];
                
                NSDate *yesterday = [NSDate dateWithTimeIntervalSinceNow: -(60.0f*60.0f*24.0f)];
                
                NSCalendar *calendar = [NSCalendar currentCalendar];
                
                NSDateComponents * componentsForYesterday=[calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:yesterday];
                
                
                NSDateComponents *componentsForToday = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:currentTime];
                
                NSDateComponents *componentsForMessageDay = [calendar components: NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:messageGetTime];
                
                
                
                if([componentsForToday day] == [componentsForMessageDay day] && [componentsForToday month] == [componentsForMessageDay month] &&[componentsForToday year] == [componentsForMessageDay year] ){
                    [presentTime appendString:[timeStamp objectAtIndex:1]];
                }
                else{
                    [presentTime appendString:[timeStamp objectAtIndex:0]];
                }
            }
            else{
                [presentTime appendString:@""];
            }
            
            if ([self.conversationList containsObject:friendName]) {
                NSInteger index = [self.friendSet indexOfObject:friendName];
                [self.populatedList replaceObjectAtIndex:index withObject:[[FriendMessage alloc]initWithFriendName:friendName andMessageTime:presentTime andLastMessage:messageContent]];
                [self.tableView reloadData];
                [self.view hideToastActivity];
            }
        }];
    }
}

- (NSString*) getProfilePictureWithUserName:(NSString*)username {
    for (PersonalProfile *profile in self.groupList) {
        if ([profile.userName isEqualToString:username]) {
            return profile.profilePicture;
        }
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.populatedList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *messageListCellIdentifier = @"MessageListCell";
    
    MessageListCell *cell = (MessageListCell*)[tableView dequeueReusableCellWithIdentifier:messageListCellIdentifier];
    
    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:messageListCellIdentifier];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MessageListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    FriendMessage* current = (FriendMessage*)[self.populatedList objectAtIndex:indexPath.row];
    //cell.textLabel.text = current.friendName;
    //cell.detailTextLabel.text = current.lastMessage;
    cell.name.text = current.friendName;
    cell.latestMessage.text = current.lastMessage;
    cell.time.text = current.messageTime;
    
    UIImage *image;
    BOOL useDefaultImage = NO;
    if (indexPath.row == 0) {
        image = [UIImage imageWithData:
                 [NSData dataWithContentsOfURL:
                  [NSURL URLWithString:
                   [[UserInfoModel sharedInstance] getCompanyLogoUrl]]]];
        
        if (!image) {
            image = [UIImage imageNamed:@"company-chat-icon.png"];
            useDefaultImage = YES;
        }
    } else {
        image = [UIImage imageWithData:
                 [NSData dataWithContentsOfURL:
                  [NSURL URLWithString:
                   [self getProfilePictureWithUserName:current.friendName]]]];
                 
        if (!image) {
            image = [UIImage imageNamed:@"personal-chat-icon.png"];
            useDefaultImage = YES;
        }
    }
    UIGraphicsBeginImageContext(CGSizeMake(45, 45));
    [image drawInRect:CGRectMake(0,0,45,45)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [cell.profileImageView setImage:newImage];
    
    if (!useDefaultImage) {
        [cell.profileImageView setBackgroundColor:[UIColor whiteColor]];
    }
    
    cell.profileImageView.layer.cornerRadius = 22.5;
    cell.profileImageView.layer.masksToBounds = YES;
    cell.profileImageView.layer.opacity = 0.6;

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageListCell *selectedCell =(MessageListCell*) [self.tableView cellForRowAtIndexPath:indexPath];
    self.selectedFriendName = [selectedCell.name text];
    
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:FRIENDLIST_COMPANY_SEGUE sender:self];
    } else {
        [self performSegueWithIdentifier:FRIENDLIST_PERSONAL_SEGUE sender:self];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:FRIENDLIST_COMPANY_SEGUE]) {
        MessageCompanyController *companyVC = segue.destinationViewController;
        companyVC.name = self.userName;
        companyVC.companyName = self.selectedFriendName;
    } else if ([segue.identifier isEqualToString:FRIENDLIST_PERSONAL_SEGUE]) {
        MessagePersonalController *personalVC = segue.destinationViewController;
        personalVC.nameSelf = self.userName;
        personalVC.nameOther = self.selectedFriendName;
    }
}

@end
