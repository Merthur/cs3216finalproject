//
//  JobListControllerController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "JobList.h"
#import "JobCartController.h"

@interface JobListController : UIViewController <UITableViewDelegate, UITableViewDataSource, loadingDelegate>

- (IBAction)backButton:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *jobListView;

@end
