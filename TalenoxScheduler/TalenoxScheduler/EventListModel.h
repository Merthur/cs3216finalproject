//
//  EventModel.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-19.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpClient.h"
#import "UserInfoModel.h"
#import "MAEvent.h"
#import "SaveEventDelegate.h"
#import "UserInfoModel.h"

@interface EventListModel : NSObject

@property (weak, nonatomic) id <loadingDelegate> loadingDelegate;

+ (EventListModel*)sharedInstance;
- (void) withLoadingDelegate:(id<loadingDelegate>)delegate;
- (NSArray*) getEventList;
- (void) addNewEvent:(MAEvent*)event withDelegate:(id<SaveEventDelegate>)delegate;
- (void) updateEventAtIndex:(NSInteger)eventID withNewEvent:(MAEvent*) event withDelegate:(id<SaveEventDelegate>)delegate;
- (void) deleteEvent:(MAEvent*)event withDelegate:(id<SaveEventDelegate>)delegate;
- (void) clearData;

@end
