//
//  UserInfoModel.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-11.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import "UserInfoModel.h"

static NSDictionary *user;
static NSDictionary *company;
static NSArray *userApps;
static NSString *token;
static NSDictionary *userEmployeeInfo;
static NSArray *employeesList;

@implementation UserInfoModel

+ (UserInfoModel *)sharedInstance{
    static dispatch_once_t once;
    static UserInfoModel *sharedInstance;
    dispatch_once(&once, ^ { sharedInstance = [[self alloc] init]; });
    
    return sharedInstance;
}

- (id)init{
    self = [super init];
    
    if (self) {
        [self clearData];
    }
    
    return self;
}

#pragma mark - initialization
- (void)userLogin:(NSDictionary *)json{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithDictionary: [json objectForKey:KEY_USER]];

    userApps = [userInfo objectForKey:KEY_USER_APP];
    [userInfo removeObjectForKey:KEY_USER_APP];
    
    userEmployeeInfo = [userInfo objectForKey:@"employee"];
    [userInfo removeObjectForKey:@"employee"];
    
    user = [[NSDictionary alloc] initWithDictionary:userInfo];
    
    token = [json objectForKey:KEY_TOKEN];
    
    [self requestUserCompany];
}

- (void) requestUserCompany {
    HttpClient *client = [HttpClient sharedInstance];

    [client getUserCompanyWithToken:token
                            success:^(NSURLSessionDataTask *task, id responseObject) {
                                if ([responseObject objectForKey:@"error"]) {
                                    NSLog(@"get company Fail");
                                } else {
                                    company = [responseObject objectForKey:KEY_COMPANY];
                                    [self requestEmployeesCacheKey];
                                }
                            }
                            failure:^(NSURLSessionDataTask *task, NSError *error) {
                                NSLog(@"Error: %@",error);
                            }];
}

- (void) requestEmployeesCacheKey {
    HttpClient *client = [HttpClient sharedInstance];
    
    [client getEmployeeCacheKeyWithToken:token
                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                     NSString *cacheKey = [[responseObject objectForKey:@"cache_keys"] objectAtIndex:0];
                                     [self requestEmployeesWithKey:cacheKey];
                                 }
                                 failure:^(NSURLSessionDataTask *task, NSError *error) {
                                     NSLog(@"Error: %@",error);
                                 }];
}

- (void) requestEmployeesWithKey:(NSString*)key {
    HttpClient *client = [HttpClient sharedInstance];
    
    [client getEmployeeListWithCacheKey:key
                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                    employeesList = responseObject;
                                    [self.loadingDelegate nextViewController];
                                }
                                failure:^(NSURLSessionDataTask *task, NSError *error) {
                                    NSLog(@"Error: %@",error);
                                }];
}

#pragma mark - get methods
- (NSDictionary*) getUserInformation{
    return user;
}

- (NSDictionary*) getCompanyInformation {
    return company;
}

- (NSArray*) getEmployeesInformation {
    return employeesList;
}

- (NSArray*) getUserApps {
    return userApps;
}

- (NSString*) getToken {
    return  token;
}

- (NSString*) getUserId {
    return [NSString stringWithFormat:@"%@", [user objectForKey:@"id"]];
}

- (NSString*) getUserEmployeeId {
    if ([userEmployeeInfo isKindOfClass:[NSDictionary class]]) {
        return [userEmployeeInfo objectForKey:@"id"];
    } else {
        return nil;
    }
}

- (NSString*) getUserEmployeeProfilePictureUrl {
    if ([userEmployeeInfo isKindOfClass:[NSDictionary class]]) {
        NSDictionary *image = [[userEmployeeInfo objectForKey:@"profile_picture"] objectForKey:@"image"];
        if ([image isKindOfClass:[NSDictionary class]]) {
            return [[[image objectForKey:@"image_file"] objectForKey:@"image_file"] objectForKey:@"url"];
        }
    }
    
    return nil;
}

- (NSString*) getCompanyLogoUrl {
    if ([company isKindOfClass:[NSDictionary class]]) {
        NSDictionary *logo = [company objectForKey:@"logo"];
        if ([logo isKindOfClass:[NSDictionary class]]) {
            NSDictionary *image = [logo objectForKey:@"image_file"];
            if ([image isKindOfClass:[NSDictionary class]]) {
                return [image objectForKey:@"url"];
            }
        }
    }
    
    return nil;
}

- (void) clearData {
    user = [[NSDictionary alloc] init];
    company = [[NSDictionary alloc] init];
    userApps = [[NSArray alloc] init];
    token = [[NSString alloc] init];
    userEmployeeInfo = [[NSDictionary alloc] init];
    employeesList = [[NSArray alloc] init];
}

@end
