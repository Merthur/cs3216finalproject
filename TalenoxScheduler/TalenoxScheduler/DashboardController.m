//
//  DashboardController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 11/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "DashboardController.h"

@interface DashboardController ()

@end

@implementation DashboardController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.userInfo = [UserInfoModel sharedInstance] ;
    
    //profile image
    NSString *profileImageURL = [self.userInfo getUserEmployeeProfilePictureUrl];
    UIImage *image;
    if (profileImageURL != nil) {
        image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: profileImageURL]]];
    } else {
        image = [UIImage imageNamed:@"avatar.png"];
        self.profileImageView.layer.opacity = 0.5;
    }
    UIGraphicsBeginImageContext(CGSizeMake(100, 100));
    [image drawInRect:CGRectMake(0,0,100,100)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.profileImageView setImage:newImage];
    self.profileImageView.layer.cornerRadius = 50.0;
    self.profileImageView.layer.masksToBounds = YES;
    self.userNameLabel.text = [[[UserInfoModel sharedInstance] getUserInformation] objectForKey:KEY_NAME];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    appWindow.rootViewController = self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString: MESSAGE_SEGUE]){
        MessageListController * messageController= segue.destinationViewController;
        messageController.userName = [[self.userInfo getUserInformation] objectForKey:KEY_NAME];
    } else if([segue.identifier isEqualToString:MESSAGE_BOX_SEGUE]){
        JobCartController *messageInboxController = segue.destinationViewController;
        messageInboxController.username = [[[UserInfoModel sharedInstance] getUserInformation] objectForKey:KEY_NAME];
        messageInboxController.jobList = [[JobList sharedInstance] getJobList];
    } else if ([segue.identifier isEqualToString:LOGOUT_SEGUE]) {
        [[UserInfoModel sharedInstance] clearData];
        [[JobList sharedInstance] clearData];
        [[HttpClient sharedInstance] clearData];
        [[EventListModel sharedInstance] clearData];
    }
}

@end
