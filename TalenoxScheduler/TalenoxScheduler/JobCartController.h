//
//  MessageInboxController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "UIView+Toast.h"
#import "JobList.h"
#import "UITouchTableView.h"
#import "JobDetailController.h"
#import "jobListCellTableViewCell.h"

@interface JobCartController : UIViewController <loadingDelegate>

@property (nonatomic) NSString *username;
@property (nonatomic, strong) NSArray* jobList;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)back:(id)sender;

@end
