//
//  JobList.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-12.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Job.h"
#import "JobApplied.h"
#import "UserInfoModel.h"
#import "HttpClient.h"

@interface JobList : NSObject

@property (weak, nonatomic) id <loadingDelegate> loadingDelegate;

+ (JobList*)sharedInstance;
- (void) withLoadingDelegate:(id<loadingDelegate>)delegate;
- (NSArray*) getJobList;
- (NSArray*) getAppliedJobList;
- (void) appliedJob:(NSInteger)jobID withAppliedID:(NSInteger) appliedID;
- (void) withdrawJobWithAppliedID:(NSInteger) appliedID;
- (void) clearData;

@end
