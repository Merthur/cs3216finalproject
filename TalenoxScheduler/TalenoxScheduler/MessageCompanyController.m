//
//  MessageController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 11/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "MessageCompanyController.h"
#import "UserInfoModel.h"

@interface MessageCompanyController ()

@property (nonatomic) BOOL newMessagesOnTop;
@property (nonatomic) MessageCell *selectedCell;

@end

@implementation MessageCompanyController {
    TSPopoverController *popoverController;
    BOOL keyboardShow;
}

@synthesize nameField;
@synthesize textField;
@synthesize tableView;
@synthesize newMessagesOnTop;

#pragma mark - Setup

// Initialization.
- (void)viewDidLoad
{
    [super viewDidLoad];
    keyboardShow = false;
    
    [textField setReturnKeyType:UIReturnKeySend];
    
    // Initialize array that will store chat messages.
    self.chat = [[NSMutableArray alloc] init];
    
    // Initialize the compnany group
    self.hostURL =[[NSMutableString alloc]initWithString:kFirechatNS];
    [self.hostURL appendString:self.companyName];
    
    
    NSLog(@"!!!!!!!!!!!!!!!!!!!!");
    NSLog(self.hostURL);
    
    // Initialize the root of our Firebase namespace.
    self.firebase = [[Firebase alloc] initWithUrl:self.hostURL];
    
    // Pick a random number between 1-1000 for our username.
    //self.name = [NSString stringWithFormat:@"Guest%d", arc4random() % 1000];
    [self.nameField setTitle:self.name forState:UIControlStateNormal];
    [self.nameField sizeToFit];
    [self.companyNameLabel setText:self.companyName];
    [self.companyNameLabel sizeToFit];
    // Decide whether or not to reverse the messages
    newMessagesOnTop = NO;
    
    // This allows us to check if these were messages already stored on the server
    // when we booted up (YES) or if they are new messages since we've started the app.
    // This is so that we can batch together the initial messages' reloadData for a perf gain.
    __block BOOL initialAdds = YES;
    [self.view makeToastActivity];
    
    [self.firebase observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
        // Add the chat message to the array.
        if (newMessagesOnTop) {
            [self.chat insertObject:snapshot.value atIndex:0];
        } else {
            [self.chat addObject:snapshot.value];
        }

        
        // Reload the table view so the new message will show up.
        if (!initialAdds) {
            [self.tableView reloadData];
            [self.view hideToastActivity];
            if([self.chat count]>MINIMUM_ROWS){
                NSIndexPath* ipath = [NSIndexPath indexPathForRow:[self.chat count]-1 inSection:0];
                [self.tableView scrollToRowAtIndexPath:ipath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
        }
    }];
    
    // Value event fires right after we get the events already stored in the Firebase repo.
    // We've gotten the initial messages stored on the server, and we want to run reloadData on the batch.
    // Also set initialAdds=NO so that we'll reload after each additional childAdded event.
    [self.firebase observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        // Reload the table view so that the intial messages show up
        [self.tableView reloadData];
        [self.view hideToastActivity];
        initialAdds = NO;
        if([self.chat count]>MINIMUM_ROWS){
            NSIndexPath* ipath = [NSIndexPath indexPathForRow:[self.chat count]-1 inSection:0];
            [self.tableView scrollToRowAtIndexPath:ipath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) viewDidAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    appWindow.rootViewController = self;
}

#pragma mark - Text field handling

// This method is called when the user enters text in the text field.
// We add the chat message to our Firebase.
- (BOOL)textFieldShouldReturn:(UITextField*)aTextField
{
    [aTextField resignFirstResponder];
    
    // This will also add the message to our local array self.chat because
    // the FEventTypeChildAdded event will be immediately fired.
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // display in 12HR/24HR (i.e. 11:25PM or 23:25) format according to User Settings
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *currentTime = [dateFormatter stringFromDate:today];
    NSLog(@"%@",currentTime);
    NSString *dateString;
    dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSLog(@"current time is !!!!!!!!!!!!!!!!!!!!");
    NSLog(dateString);
    [[self.firebase childByAutoId] setValue:@{@"name" : self.name, @"text": aTextField.text, @"time": dateString}];
    
    [aTextField setText:@""];
    return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    // We only have one section in our table view.
    return 1;
}

- (NSInteger)tableView:(UITableView*)table numberOfRowsInSection:(NSInteger)section
{
    // This is the number of chat messages.
    return [self.chat count];
}

// This method changes the height of the text boxes based on how much text there is.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* chatMessage = [self.chat objectAtIndex:indexPath.row];
    NSString *thisMessage = chatMessage[@"text"];
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:TABLE_CELL_FONT]};
    CGSize size = [thisMessage boundingRectWithSize:CGSizeMake(TABLE_CELL_WIDTH, 0)
                                            options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                         attributes:attribute
                                            context:nil].size;
    return size.height+TABLE_CELL_MARGIN;
}

- (UITableViewCell*)tableView:(UITableView*)table cellForRowAtIndexPath:(NSIndexPath *)index
{
    static NSString *CellIdentifier = @"MessageCell";
    MessageCell *cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary* chatMessage = [self.chat objectAtIndex:index.row];
    
    cell.messageView.text = chatMessage[@"text"];
    
    CGFloat fixedWidth = cell.messageView.frame.size.width;
    CGSize newSize = [cell.messageView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = cell.messageView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    cell.messageView.frame = newFrame;
    
    cell.nameLabel.text = chatMessage[@"name"];
    
    if ([cell.nameLabel.text isEqualToString:[[[UserInfoModel sharedInstance] getUserInformation] objectForKey:KEY_NAME]]) {
        
        cell.nameLabel.frame = CGRectMake(0, 0, 0, 0);
        cell.messageView.textAlignment = NSTextAlignmentLeft;
        cell.messageView.frame = CGRectMake(68, 9, newFrame.size.width, newFrame.size.height);
        cell.messageView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
    } else {
        cell.nameLabel.textAlignment = NSTextAlignmentLeft;
        cell.messageView.textAlignment = NSTextAlignmentLeft;
        cell.nameLabel.frame = CGRectMake(8, 2, 210, 16);
        cell.messageView.frame = CGRectMake(8, 18, newFrame.size.width, newFrame.size.height);
        cell.messageView.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [textField resignFirstResponder];
    self.selectedCell = (MessageCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    if (![self.selectedCell.nameLabel.text isEqualToString:self.name]) {
        UIView *newEventVC = [[UIView alloc] init];
        
        UIButton *button = [[UIButton alloc] init];
        button.frame = CGRectMake(0, 0, 40, 20);
        NSString *title = [@"Chat with " stringByAppendingString:self.selectedCell.nameLabel.text];
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor]
         forState:UIControlStateNormal];
        [button sizeToFit];
        newEventVC.frame = button.frame;
        [button addTarget:self action:@selector(chatWith:) forControlEvents:UIControlEventTouchUpInside];
        [newEventVC addSubview: button];
        
        popoverController = [[TSPopoverController alloc] initWithView:newEventVC];
        popoverController.cornerRadius = 5;
        popoverController.popoverBaseColor = [UIColor colorWithRed:80/255.0 green:194/255.0 blue:169/255.0 alpha:1];
        popoverController.popoverGradient= NO;
        [popoverController showPopoverWithCell:self.selectedCell];

    }
}

- (void) chatWith: (id) sender {
    [popoverController dismissPopoverAnimatd:NO];
    [self performSegueWithIdentifier:COMPANY_PERSONAL_SEGUE sender:self];
}

#pragma mark - Navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:COMPANY_PERSONAL_SEGUE]) {
        MessagePersonalController *personalVC = segue.destinationViewController;
        personalVC.nameSelf = self.name;
        personalVC.nameOther = self.selectedCell.nameLabel.text;
    } else  if ([segue.identifier isEqualToString:COMPANY_INFO_SEGUE]) {
        GroupInfoController *infoVC = segue.destinationViewController;
        infoVC.groupName = self.companyName;
    }
}

- (IBAction)backPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboard handling
// Subscribe to keyboard show/hide notifications.
- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillShow:)
     name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(keyboardWillHide:)
     name:UIKeyboardWillHideNotification object:nil];
}

// Unsubscribe from keyboard show/hide notifications.
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Setup keyboard handlers to slide the view containing the table view and
// text field upwards when the keyboard shows, and downwards when it hides.
- (void)keyboardWillShow:(NSNotification*)notification
{
    if (!keyboardShow) {
        keyboardShow = YES;
        [self moveView:[notification userInfo] up:YES];
    }
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    if (keyboardShow) {
        keyboardShow = NO;
        [self moveView:[notification userInfo] up:NO];
    }
}


- (void)moveView:(NSDictionary*)userInfo up:(BOOL)up
{
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]
     getValue:&keyboardEndFrame];
    
    UIViewAnimationCurve animationCurve;
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]
     getValue:&animationCurve];
    
    NSTimeInterval animationDuration;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey]
     getValue:&animationDuration];
    
    // Get the correct keyboard size to we slide the right amount.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
    int y = keyboardFrame.size.height * (up ? -1 : 1);
    self.view.frame = CGRectOffset(self.view.frame, 0, y);
    
    [UIView commitAnimations];
}

// This method will be called when the user touches on the tableView, at
// which point we will hide the keyboard (if open). This method is called
// because UITouchTableView.m calls nextResponder in its touch handler.
- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    if ([textField isFirstResponder]) {
        [textField resignFirstResponder];
    }
}

@end

