//
//  MessageCell.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 22/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        CGRect frame = CGRectMake(8, 2, 210, 16);
        self.nameLabel = [[UILabel alloc] initWithFrame:frame];
        self.nameLabel.textColor = [UIColor whiteColor];
        self.nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
        self.nameLabel.textAlignment = NSTextAlignmentLeft;
        
        [self addSubview: self.nameLabel];
        
        frame = CGRectMake(8, 18, 210, 30);
        self.messageView = [[UITextView alloc] initWithFrame:frame];
        self.messageView.textColor = [UIColor colorWithRed:59/255.0 green:154/255.0 blue:153/255.0 alpha:1];
        self.messageView.font = [UIFont fontWithName:@"Arial" size:17.0];
        self.messageView.backgroundColor = [UIColor whiteColor];
        self.messageView.scrollEnabled = NO;
        [self.messageView setUserInteractionEnabled:NO];
        [self.messageView.layer setCornerRadius:10.0f];
        
        [self addSubview: self.messageView];
        
        [self setBackgroundColor:[UIColor clearColor]];

    }
    
    return self;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:NO];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    if(selected){
        self.messageView.layer.borderWidth = 1.0f;
        self.messageView.layer.borderColor = [UIColor blackColor].CGColor;
    }
    else{
        self.messageView.layer.borderWidth = 0.0f;
        self.messageView.layer.borderColor = nil;
        
    }

    // Configure the view for the selected state
}

@end
