//
//  NewEventController.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-22.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SaveEventDelegate.h"
#import "MAEvent.h"
#import "TSPopoverController.h"
#import "HttpClient.h"
#import "UIView+Toast.h"
#import "Constants.h"

@interface NewEventController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>;
@property (strong, nonatomic) IBOutlet UITextField *startTimeInput;
@property (strong, nonatomic) IBOutlet UITextField *endTimeInput;
@property (strong, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (strong, nonatomic) IBOutlet UIDatePicker *startTiemPicker;
@property (strong, nonatomic) NSArray *RecurrenceOption;
@property (strong, nonatomic) IBOutlet UILabel *endTimeLable;
@property (strong, nonatomic) IBOutlet UIDatePicker *endTimePicker;
@property (strong, nonatomic) IBOutlet UITextField *recurrenceInput;
@property (strong, nonatomic) IBOutlet UITextField *times;

@property (weak, nonatomic) IBOutlet UIPickerView *Recurrence;

@property (nonatomic) TSPopoverController *controller;
@property (nonatomic) MAEvent *event;

@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *backButton;

@property (weak, nonatomic) id <SaveEventDelegate> saveDelegate;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;
@end
