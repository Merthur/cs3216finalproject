//
//  EventUpdateViewDelegate.h
//  TalenoxScheduler
//
//  Created by io on 22/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#ifndef TalenoxScheduler_EventUpdateViewDelegate_h
#define TalenoxScheduler_EventUpdateViewDelegate_h

@protocol EventUpdateViewDelegate <NSObject>

// Called when the navigation controller shows a new top view controller via a push, pop or setting of the view controller stack.
- (void)viewUpdated;

@end

#endif
