//
//  HttpClient.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-11.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "HttpConstant.h"
#import "UserInfoModel.h"
#import "MAEvent.h"
#import <AFHTTPRequestOperation.h>
#import <AFHTTPSessionManager.h>
#import <AFHTTPRequestOperationManager.h>

@class HttpClient;

@interface HttpClient :AFHTTPSessionManager
+ (HttpClient *) sharedInstance;

//// Authentication ////
- (void)userLogin:(NSString*)account Password:(NSString*) password
          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) getUserCompanyWithToken:(NSString*)token
                         success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                         failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) getEmployeeCacheKeyWithToken:(NSString*)token
                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) getEmployeeListWithCacheKey:(NSString*)token
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) createEventWithSlot:(MAEvent*) event
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) updateEventWithSlot:(MAEvent*) event
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) deleteEventWithSlot:(MAEvent*) event
                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) getAvailabilityWithEmployeeID:(NSString*) employID
                               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) getJobListWithEmployeeID:(NSString*)employID
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) getApplicationHistoryWithEmployeeID:(NSString*)employID
                                     success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                     failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) applyJobWithJobID:(NSInteger)jobID
                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) withdrawJobWithAppliedID:(NSInteger)appliedID
                          success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void) clearData;

@end
