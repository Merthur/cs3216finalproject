//
//  main.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-5.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
