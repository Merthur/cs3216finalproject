//
//  MessageListCell.m
//  TalenoxScheduler
//
//  Created by Wen Yiran on 10/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "MessageListCell.h"

@implementation MessageListCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor=[UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
