//
//  customizedSegue.m
//  TalenoxScheduler
//
//  Created by io on 22/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "customizedSegue.h"
#import "DayViewExampleController.h"

@implementation customizedSegue

- (void) perform {
    //DayViewExampleController *source = (DayViewExampleController*) self.sourceViewController;
    
    UIViewController *destinationVC = (UIViewController*) self.destinationViewController;
    UIViewController *sourceVC = (UIViewController*) self.sourceViewController;

    
    [(DayViewExampleController*)destinationVC updateEventList];
    [sourceVC.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
