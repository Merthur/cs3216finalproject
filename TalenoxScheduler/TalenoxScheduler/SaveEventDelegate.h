//
//  SaveEventDelegate.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 22/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#ifndef TalenoxScheduler_SaveEventDelegate_h
#define TalenoxScheduler_SaveEventDelegate_h
#import "MAEvent.h"

@protocol SaveEventDelegate <NSObject>

-(id) saveEvent:(MAEvent*) event;
-(void) databaseUpdate:(NSInteger) command andResult: (NSInteger) result;


@end

#endif
