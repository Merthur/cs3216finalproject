//
//  JobDetailController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "Constants.h"
#import "UIView+Toast.h"


@interface JobDetailController : UIViewController

@property (nonatomic) NSString *userName;
@property (nonatomic) NSString *ownerName;
@property (nonatomic) NSDate *startTime;
@property (nonatomic) NSDate *endTime;
@property (nonatomic) NSString *shiftName;
@property (nonatomic) NSString *location;
@property (nonatomic) NSInteger jobID;
@property (nonatomic) NSInteger appliedID;
@property (nonatomic) NSString *jobDescription;
@property (nonatomic) NSString *buttonMessage;

- (IBAction)applyAJob:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UITextView *jobDescriptionLabel;
@property (strong, nonatomic) IBOutlet UIButton *applyButton;
@property (strong, nonatomic) IBOutlet UILabel *jobTitle;

- (IBAction)backButton:(id)sender;


@end
