//
//  EventDetailController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 22/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "EventDetailController.h"

@interface EventDetailController ()

@end

@implementation EventDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.eventName setText: self.eventname];
    [self.startPicker setDate:self.event.start];
    [self.endPicker setDate:self.event.end];
    //NSLog(self.event.start);
    //NSLog(self.event.end);
    
    [self.endPicker setHidden:YES];
    
    [self.startTimeInput addTarget:self
                            action:@selector(showStartTimePicker:)
                  forControlEvents:UIControlEventEditingDidBegin];
    [self.endTimeInput addTarget:self
                          action:@selector(showEndTimePicker:)
                forControlEvents:UIControlEventEditingDidBegin];
    [self.startPicker addTarget:self
                             action:@selector(startTimeChanged:)
                   forControlEvents:UIControlEventValueChanged];
    [self.endPicker addTarget:self
                           action:@selector(endTimeChanged:)
                 forControlEvents:UIControlEventValueChanged];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *currentStartTimeString = [dateFormatter stringFromDate:self.event.start];
    NSString *currentEndTimeString = [dateFormatter stringFromDate:self.event.end];
    [self.startTimeInput setText:currentStartTimeString];
    [self.endTimeInput setText:currentEndTimeString];
    
    [self.updateButton.layer setCornerRadius:BUTTON_RADIUS];
    [self.updateButton setClipsToBounds:YES];
    [self.backButton.layer setCornerRadius:BUTTON_RADIUS];
    [self.backButton setClipsToBounds:YES];
    [self.deleteButton.layer setCornerRadius:BUTTON_RADIUS];
    [self.deleteButton setClipsToBounds:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(bool) checkCrash:(MAEvent *)event currentID:(NSInteger) ID{
    BOOL flag=false;
    NSArray *eventList;
    eventList=[[EventListModel sharedInstance] getEventList];
    for (MAEvent * object in eventList) {
        if (object.event_id==ID) continue;
        flag=true;
        if (object.start >= event.end) flag=false;
        if (object.end <= event.start) flag=false;
        if (flag) return true;
    }
    
    return false;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)Delete:(id)sender {
    NSLog(@"delete information ready to sent");
    //self.eventList=[[EventListModel sharedInstance] getEventList];
    //self.event.title=@"Deleted";
    [[EventListModel sharedInstance] deleteEvent:self.event withDelegate:self];
    self.command=TWO;
    NSLog(@"delete information sent");
    [self.controller dismissPopoverAnimatd:YES];
}

- (IBAction)update:(id)sender {
    if(self.deletedPressed){
        [self.view makeToast:DELETED_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        return;
    }
    NSDate *tmp1=self.event.start;
    NSDate *tmp2=self.event.end;
    self.event.start = [self.startPicker date];
    self.event.end = [self.endPicker date];
    if ([self checkCrash:self.event currentID:self.event.event_id]){
        [self.view makeToast:CLASH_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        self.event.start=tmp1;
        self.event.end=tmp2;
        return;
    }else{
        [[EventListModel sharedInstance] updateEventAtIndex:self.event.event_id withNewEvent:self.event withDelegate:self];
        NSLog(@"send update information");
        self.command=3;
        // Update function here
    }
    
    [self.controller dismissPopoverAnimatd:YES];
}

- (IBAction)back:(id)sender {
     [self.controller dismissPopoverAnimatd:YES];
}

-(id)init{
    self = [super init];
    self.eventName.text = @"name";

    if(self){
        self.deletedPressed =false;
    }
    
    return self;
}

-(id)initWithEventName:(NSString *)name andStartTime:(NSString *)stime andEndTime:(NSString *)etime{
    self = [super init];
    if(self){
        self.deletedPressed = false;
        self.eventName.text = name;
    }
    return self;
}

-(void) showStartTimePicker:(id)sender {
    [self.startTimeInput resignFirstResponder];
    [self.startPicker setHidden:NO];
    [self.endPicker setHidden:YES];

    NSLog(@"showStartTimePicker");
}

-(void) showEndTimePicker:(id)sender {
    [self.endTimeInput resignFirstResponder];
    [self.endPicker setHidden:NO];
    [self.startPicker setHidden:YES];
   
    NSLog(@"showEndTimePicker");
}

-(void) endTimeChanged:(id)sender {
    NSDate *endTime = [self.endPicker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMATE;
    
    [self.endTimeInput setText:[dateFormatter stringFromDate:endTime]];
}

-(void) startTimeChanged:(id)sender {
    NSDate *startTime = [self.startPicker date];
    NSDate *endTime = [self.endPicker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMATE;
    [self.startTimeInput setText:[dateFormatter stringFromDate:startTime]];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsForFirstDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:startTime];
    
    NSDateComponents *componentsForSecondDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:endTime];
    
    if ([componentsForFirstDate day] == [componentsForSecondDate day] && [componentsForFirstDate month] == [componentsForSecondDate month] &&[componentsForFirstDate year] == [componentsForSecondDate year] ){
        
    }
    else{
        self.endPicker.date = startTime;
        endTime = startTime;
        [self.endTimeInput setText:[dateFormatter stringFromDate:endTime]];
    }
    
}

-(id) saveEvent:(MAEvent*) event {
    return nil;
}

-(void) databaseUpdate:(NSInteger) command andResult:(NSInteger)result{
    NSLog(@"Delegate called");
    if (result==ZERO){
        [self.view makeToast:DELETED_FAILED_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        
    }else if (self.command==TWO){
        [self.delegate viewUpdated];
        [self.view makeToast:DELETED_SUCCESS_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        self.deletedPressed = true;

    }else{
        self.event.start=self.startPicker.date;
        self.event.title=self.eventName.text;
        self.event.end=self.endPicker.date;
        [self.delegate viewUpdated];
        [self.view makeToast:UPDATE_SUCCESS duration:POPUP_DURATION position:POSITION_CENTER];
    }
}
@end
