/*
 * Copyright (c) 2010-2012 Matias Muhonen <mmu@iki.fi>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "DayViewExampleController.h"
#import "MAEvent.h"
#import "MAEventKitDataSource.h"

// Uncomment the following line to use the built in calendar as a source for events:
//#define USE_EVENTKIT_DATA_SOURCE 1

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]

@interface DayViewExampleController(PrivateMethods)
@property (readonly) MAEvent *event;
@property (readonly) MAEventKitDataSource *eventKitDataSource;
@end

@implementation DayViewExampleController

-(bool) checkCrash:(MAEvent *)event{
    BOOL flag=false;
    for (MAEvent * object in self.eventList) {
        flag=true;
        if (object.start >= event.end) flag=false;
        if (object.end <= event.start) flag=false;
        if (flag) return true;
    }
    
    return false;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden: YES];
    self.eventList = [[NSArray alloc] init];
    self.eventList = [[EventListModel sharedInstance] getEventList];
    [((MADayView *) self.view) reloadData];
    self.RecurrenceEvent = [[NSMutableArray alloc]init];
    self.RecurrenceEventForDay = [[NSMutableArray alloc]init];
    self.RecurrenceEventForDaySP = [[NSMutableArray alloc]init];
    MADayView *dayView = (MADayView *) self.view;
    
	/* The default is not to autoscroll, so let's override the default here */
	dayView.autoScrollToFirstEvent = YES;
    
}

/* Implementation for the MADayViewDataSource protocol */

static NSDate *date = nil;

#ifdef USE_EVENTKIT_DATA_SOURCE

- (NSArray *)dayView:(MADayView *)dayView eventsForDate:(NSDate *)startDate {
    return [self.eventKitDataSource dayView:dayView eventsForDate:startDate];
}

#else
- (NSArray *)dayView:(MADayView *)dayView eventsForDate:(NSDate *)startDate {
    
    return self.eventList;
}
#endif

-(id) saveEvent:(MAEvent *)event{
    if (self.event.event_id == -ONE){
        [self.view makeToast:CONNECTION_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        return nil;
    }
    if ([self checkCrash:event]==false){
        [[EventListModel sharedInstance] addNewEvent:event withDelegate:self];
    }else{
        [self.view makeToast:CLASH_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        
    }
    [((MADayView *) self.view) reloadData];
    
    [self.RecurrenceEvent init];
    [self.RecurrenceEventForDay init];
    [self.RecurrenceEventForDaySP init];
    
    if (event.recurrence==ZERO){
        [self.RecurrenceEvent addObject:event];
    }
    else if (event.recurrence==TWO){
        [self.RecurrenceEventForDay addObject:event];
    }else if(event.recurrence==ONE){
        [self.RecurrenceEventForDaySP addObject:event];
        
    }
    
    
    for (NSInteger i=0; i<[self.RecurrenceEvent count]; i++){
        
        MAEvent *current_event=[self.RecurrenceEvent objectAtIndex:i];
        for (NSInteger j=0;j<event.times-1;j++){
            MAEvent *new_event = [[MAEvent alloc] init];
            
            NSTimeInterval secondsPer7Day =(j+1) * SEVEN_DAY_SECONDS;
            [new_event setStart:[current_event.start dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setEnd:[current_event.end dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setTextColor:[current_event textColor]  ];
            [new_event setBackgroundColor:[current_event backgroundColor]  ];
            
            if (new_event.event_id==-ONE){
                [self.view makeToast:CONNECTION_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
                return nil;
            }
            if ([self checkCrash:new_event]==false){
                [[EventListModel sharedInstance] addNewEvent:new_event withDelegate:self];
            }else{
                [self.view makeToast:CLASH_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
                
            }
        }
    }
    
    for (NSInteger i=0;i<[self.RecurrenceEventForDay count];i++){
        
        MAEvent *current_event=[self.RecurrenceEventForDay objectAtIndex:i];
        
        for (NSInteger j=0;j<event.times-1;j++){
            MAEvent *new_event = [[MAEvent alloc] init];
            
            NSTimeInterval secondsPer7Day =(j + ONE)* ONE_DAY_SECONDS;
            
            [new_event setStart:[current_event.start dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setEnd:[current_event.end dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setTextColor:[current_event textColor]  ];
            [new_event setBackgroundColor:[current_event backgroundColor]];
            
            if ([self checkCrash:new_event]==false){
                [[EventListModel sharedInstance] addNewEvent:new_event withDelegate:self];
            }else{
                [self.view makeToast:CLASH_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
                
            }
        }
    }
    for (NSInteger i=0;i<[self.RecurrenceEventForDaySP count];i++){
        MAEvent *current_event=[self.RecurrenceEventForDaySP objectAtIndex:i];
        
        for (NSInteger j=0;j<event.times-1;j++){
            MAEvent *new_event = [[MAEvent alloc] init];
            
            NSTimeInterval secondsPer7Day =(j+1)* ONE_DAY_SECONDS;
            [new_event setStart:[current_event.start dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setEnd:[current_event.end dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setTextColor:[current_event textColor]  ];
            [new_event setBackgroundColor:[current_event backgroundColor]  ];
            NSDateFormatter *weekday = [[NSDateFormatter alloc] init] ;
            [weekday setDateFormat: WEEKDAY_FORMATE];
           // NSLog(@"The day of the week is: %@", [weekday stringFromDate:new_event.start]);
            if ([[weekday stringFromDate:new_event.start] isEqualToString:SUNDAY]) continue;
            if ([[weekday stringFromDate:new_event.start] isEqualToString:SATURDAY]) continue;
            if ([self checkCrash:new_event]==false){
                [[EventListModel sharedInstance] addNewEvent:new_event withDelegate:self];
            }else{
                [self.view makeToast:CLASH_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
            }
        }
        
    }
    return nil;
}

- (void) updateEventList{
    self.eventList=[[EventListModel sharedInstance] getEventList];

    [((MADayView *) self.view) reloadData];
}

- (MAEvent *)event {
	static int counter;
    
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	
	[dict setObject:[NSString stringWithFormat:@"number %i", counter++] forKey:@"test"];
	
	unsigned int r = arc4random() % 24;
	int rr = arc4random() % 3;
	
	MAEvent *event = [[MAEvent alloc] init];
	event.backgroundColor = [UIColor colorWithRed:225/255.0 green:236/255.0 blue:187/255.0 alpha:0.6];
	event.textColor = [UIColor whiteColor];
	event.allDay = NO;
	event.userInfo = dict;
	
	if (rr == 0) {
		event.title = @"Event lorem ipsum es dolor test. This a long text, which should clip the event view bounds.";
	} else if (rr == 1) {
		event.title = @"Foobar.";
	} else {
		event.title = @"Dolor test.";
	}
	
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:date];
	[components setHour:r];
	[components setMinute:0];
	[components setSecond:0];
	
	event.start = [CURRENT_CALENDAR dateFromComponents:components];
	
	[components setHour:r+rr];
	[components setMinute:0];
	
	event.end = [CURRENT_CALENDAR dateFromComponents:components];
	
	return event;
}

- (MAEventKitDataSource *)eventKitDataSource {
    if (!_eventKitDataSource) {
        _eventKitDataSource = [[MAEventKitDataSource alloc] init];
    }
    return _eventKitDataSource;
}

/* Implementation for the MADayViewDelegate protocol */

- (void) backToDashboard {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)dayView:(MADayView *)dayView eventTapped:(MAEvent *)event {
    EventDetailController *eventDetailController = [self.storyboard instantiateViewControllerWithIdentifier:@"EVENT_DETAIL"];
    eventDetailController.eventname=event.title;
    eventDetailController.event = event;
    eventDetailController.delegate = self;
    eventDetailController.view.frame = CGRectMake(0,0, 320, 350);
    
    TSPopoverController *popoverController = [[TSPopoverController alloc] initWithContentViewController:eventDetailController];
    popoverController.cornerRadius = 5;
    popoverController.titleText = @"Edit Free Slot";
    popoverController.titleFont = [UIFont boldSystemFontOfSize:18];
    popoverController.popoverBaseColor = [UIColor colorWithRed:66/255.0 green:135/255.0 blue:126/255.0 alpha:0.3];
    popoverController.popoverGradient= NO;
    [popoverController showPopoverWithRect:CGRectMake(200, 35, 20, 20)];
    eventDetailController.controller = popoverController;
    
    [self updateEventList];
}

- (void)viewUpdated{
    NSLog(@"called!!!");
    [self updateEventList];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void) viewDidAppear:(BOOL)animated {
    [self updateEventList];
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    appWindow.rootViewController = self.navigationController;
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)databaseUpdate:(NSInteger)command andResult:(NSInteger)result{
    if (result==0){
        [self.view makeToast:@"Operation Failed Due to Connection Problem"];
    }else{
        [self updateEventList];
    }
}
- (void)CreateSlot:(CGPoint)point initDate:(NSDate*) initDate offsetx:(NSInteger) x offsety:(NSInteger) y{
    NSLog(@"Successful");
    NSLog(@"%i %i",x,y);
    NSLog(initDate.description);
    NSTimeInterval seconds =y*60*60;
    
    NSDate* newdate= [initDate dateByAddingTimeInterval: seconds];
    NSDate* newdate2= [initDate dateByAddingTimeInterval: seconds+3600];
    
    NSLog([newdate description]);
    NewEventController *newEventVC = [[NewEventController alloc] init];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm";
    newEventVC.startTiemPicker.date=newdate;
    newEventVC.saveDelegate = self;
    [newEventVC.startTimeInput setText:[dateFormatter stringFromDate:newdate]];
    newEventVC.endTimePicker.date=newdate2;
    [newEventVC.endTimeInput setText:[dateFormatter stringFromDate:newdate2]];
    NSLog([dateFormatter stringFromDate:newdate2]);
    newEventVC.view.frame = CGRectMake(0,0, 320, 400);
    TSPopoverController *popoverController = [[TSPopoverController alloc] initWithContentViewController:newEventVC];
    newEventVC.controller = popoverController;
    popoverController.cornerRadius = 5;
    popoverController.titleText = @"Add Free Slot";
    popoverController.popoverBaseColor = [UIColor colorWithRed:80/255.0 green:194/255.0 blue:169/255.0 alpha:1];
    popoverController.popoverGradient= NO;
    //    popoverController.arrowPosition = TSPopoverArrowPositionHorizontal;
    [popoverController showPopoverWithRect:CGRectMake(295, 35, 20, 20)];
    newEventVC.startTiemPicker.date=newdate;
    [newEventVC.startTimeInput setText:[dateFormatter stringFromDate:newdate]];
    newEventVC.endTimePicker.date=newdate2;
    [newEventVC.endTimeInput setText:[dateFormatter stringFromDate:newdate2]];
}


@end
