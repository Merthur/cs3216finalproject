/*
 * Copyright (c) 2010-2012 Matias Muhonen <mmu@iki.fi>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "WeekViewExampleController.h"
#import "MAWeekView.h"
#import "MAEvent.h"
#import "MAEventKitDataSource.h"
#import "EventDetailController.h"
// Uncomment the following line to use the built in calendar as a source for events:
//#define USE_EVENTKIT_DATA_SOURCE 1

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]

@interface WeekViewExampleController(PrivateMethods)
@property (readonly) MAEvent *event;
@property (readonly) MAEventKitDataSource *eventKitDataSource;
@end

@implementation WeekViewExampleController

-(bool) checkCrash:(MAEvent *)event{
    BOOL flag=false;
    for (MAEvent * object in self.eventList) {
        flag=true;
        if (object.start >= event.end) flag=false;
        if (object.end <= event.start) flag=false;
        if (flag) return true;
    }
    
    return false;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

/* Implementation for the MAWeekViewDataSource protocol */

#ifdef USE_EVENTKIT_DATA_SOURCE

- (NSArray *)weekView:(MAWeekView *)weekView eventsForDate:(NSDate *)startDate {
    return [self.eventKitDataSource weekView:weekView eventsForDate:startDate];
}

#else

- (NSArray *)weekView:(MAWeekView *)weekView eventsForDate:(NSDate *)startDate {

    return self.eventList;
}

#endif

-(id) saveEvent:(MAEvent *)event{
    NSLog(@"come in weekview save event delegate");
    
    NSDateComponents *component = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:event.start];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"<your date format goes here"];
    NSDate *date = event.start;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    NSDateComponents *newCom = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    NSInteger year =[newCom year];
    NSInteger month = [newCom month];
    NSInteger day =[newCom day];
    
    [component setYear:year];
    [component setMonth:month];
    [component setDay:day];
    [component setHour:hour];
    [component setMinute:minute];
    [component setSecond:0];
    
    event.start = [CURRENT_CALENDAR dateFromComponents:component];
    
    date =event.end;
    components = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    
    hour = [components hour];
    minute = [components minute];
    day = [components day];
    month = [components month];
    year = [components year];
    
    [component setHour:hour];
    [component setMinute:minute];
    [component setDay:day];
    [component setMonth:month];
    [component setYear:year];
    [self.RecurrenceEvent init];
    
    [self.RecurrenceEventForDay init];
    [self.RecurrenceEventForDaySP init];
    event.end = [CURRENT_CALENDAR dateFromComponents:component];
    if (event.recurrence==ZERO){
        [self.RecurrenceEvent addObject:event];
    }
    if (event.recurrence==TWO){
        [self.RecurrenceEventForDay addObject:event];
    }else if(event.recurrence==ONE){
        [self.RecurrenceEventForDaySP addObject:event];
        
    }
    
    if ([self checkCrash:event]==false){
        [[EventListModel sharedInstance] addNewEvent:event withDelegate:self];
    }else{
        [self.view makeToast:@"There already exists an event in your chosen slot"duration:3 position:@"center"];
        
    }
    
    [((MAWeekView *) self.view) reloadData];
    for (NSInteger i=0;i<[self.RecurrenceEvent count];i++){
        MAEvent *current_event=[self.RecurrenceEvent objectAtIndex:i];
        
        for (NSInteger j=0;j<event.times-1;j++){
            MAEvent *new_event = [[MAEvent alloc] init];
            
            NSTimeInterval secondsPer7Day =(j+1)*SEVEN_DAY_SECONDS;
            [new_event setStart:[current_event.start dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setEnd:[current_event.end dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setTextColor:[current_event textColor]  ];
            [new_event setBackgroundColor:[current_event backgroundColor]  ];
            
            
            if ([self checkCrash:new_event]==false){
                [[EventListModel sharedInstance] addNewEvent:new_event withDelegate:self];
            }else{
                [self.view makeToast:@"There already exists an event in your chosen slot" duration:3 position:@"center"];
                
            }
        }
    }
    for (NSInteger i=0;i<[self.RecurrenceEventForDay count];i++){
        MAEvent *current_event=[self.RecurrenceEventForDay objectAtIndex:i];
        
        for (NSInteger j=0;j<event.times-1;j++){
            MAEvent *new_event = [[MAEvent alloc] init];
            
            NSTimeInterval secondsPer7Day =(j+1)* ONE_DAY_SECONDS;
            [new_event setStart:[current_event.start dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setEnd:[current_event.end dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setTextColor:[current_event textColor]  ];
            [new_event setBackgroundColor:[current_event backgroundColor]  ];
            if ([self checkCrash:new_event]==false){
                [[EventListModel sharedInstance] addNewEvent:new_event withDelegate:self];
            }else{
                [self.view makeToast:@"There already exists an event in your chosen slot" duration:3 position:@"center"];
            }
        }
        
    }
    for (NSInteger i=0;i<[self.RecurrenceEventForDaySP count];i++){
        MAEvent *current_event=[self.RecurrenceEventForDaySP objectAtIndex:i];
        
        for (NSInteger j=0;j<event.times-1;j++){
            MAEvent *new_event = [[MAEvent alloc] init];
            
            NSTimeInterval secondsPer7Day =(j+1)* ONE_DAY_SECONDS;
            [new_event setStart:[current_event.start dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setEnd:[current_event.end dateByAddingTimeInterval: secondsPer7Day]];
            [new_event setTextColor:[current_event textColor]  ];
            [new_event setBackgroundColor:[current_event backgroundColor]  ];
            NSDateFormatter *weekday = [[NSDateFormatter alloc] init] ;
            [weekday setDateFormat: WEEKDAY_FORMATE];
            NSLog(@"The day of the week is: %@", [weekday stringFromDate:new_event.start]);
            if ([[weekday stringFromDate:new_event.start] isEqualToString:SUNDAY]) continue;
            if ([[weekday stringFromDate:new_event.start] isEqualToString:SATURDAY]) continue;
            if ([self checkCrash:new_event]==false){
                [[EventListModel sharedInstance] addNewEvent:new_event withDelegate:self];
            }else{
                [self.view makeToast:CLASH_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
            }
        }
        
    }
    [((MAWeekView *) self.view) reloadData];
    return nil;
}

- (MAEvent *)event {
	static int counter;
	
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	
	[dict setObject:[NSString stringWithFormat:@"number %i", counter++] forKey:@"test"];
	
	MAEvent *event = [[MAEvent alloc] init];
	event.backgroundColor = [UIColor colorWithRed:225/255.0 green:236/255.0 blue:187/255.0 alpha:0.6];
    event.textColor = [UIColor whiteColor];
	event.allDay = NO;
	event.userInfo = dict;
	return event;
}

- (MAEventKitDataSource *)eventKitDataSource {
    if (!_eventKitDataSource) {
        _eventKitDataSource = [[MAEventKitDataSource alloc] init];
    }
    return _eventKitDataSource;
}

/* Implementation for the MAWeekViewDelegate protocol */
- (void) backToDashboard {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    UIViewController *dashboardVC = [self.storyboard instantiateViewControllerWithIdentifier:DASHBOARD_CONTROLLER];
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    appWindow.rootViewController = dashboardVC;
}

- (void) updateEventList{
    NSLog(@"%lu",(unsigned long)[self.RecurrenceEvent count]);

    self.eventList=[[EventListModel sharedInstance] getEventList];
    [((MAWeekView *) self.view) reloadData];
}
- (void)weekView:(MAWeekView *)weekView eventTapped:(MAEvent *)event {
    EventDetailController *eventDetailController = [self.storyboard instantiateViewControllerWithIdentifier:EVENT_DETAIL_SEGUE];
    
    eventDetailController.eventname=event.title;
    eventDetailController.event = event;
    eventDetailController.delegate = self;
    eventDetailController.view.frame = CGRectMake(EVENT_DETAIL_FRAME_X, EVENT_DETAIL_FRAME_Y, EVENT_DETAIL_FRAME_WIDTH, EVENT_DETAIL_FRAME_HEIGHT);
    
    TSPopoverController *popoverController = [[TSPopoverController alloc] initWithContentViewController:eventDetailController];
    popoverController.cornerRadius = 5;
    popoverController.titleText = @"Edit Free Slot";
    popoverController.titleFont = [UIFont boldSystemFontOfSize:18];
    popoverController.popoverBaseColor = [UIColor colorWithRed:66/255.0 green:135/255.0 blue:126/255.0 alpha:0.3];
    popoverController.popoverGradient= NO;
    [popoverController showPopoverWithRect:CGRectMake(POPOVER_FRAME_X, POPOVER_FRAME_Y, POPOVER_FRAME_WIDTH, POPOVER_FRAME_HEIGHT)];
    eventDetailController.controller = popoverController;
    
    [self updateEventList];
}

- (void)weekView:(MAWeekView *)weekView eventDragged:(MAEvent *)event {
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:event.start];
	NSString *eventInfo = [NSString stringWithFormat:@"Event dragged to %02i:%02i. Userinfo: %@", [components hour], [components minute], [event.userInfo objectForKey:@"test"]];
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:event.title
                                                    message:eventInfo delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
}

- (void)viewUpdated{
    NSLog(@"called!!!");
    [self updateEventList];
}

- (void) nextViewController {
    [self updateEventList];
    [self.view hideToastActivity];
}

- (void) viewDidLoad {
    [self.view makeToastActivity];
    [[EventListModel sharedInstance] withLoadingDelegate:self];
    self.eventList = [[NSMutableArray alloc]init];
    
    [self updateEventList];
    
    self.RecurrenceEvent = [[NSMutableArray alloc]init];
    self.RecurrenceEventForDay = [[NSMutableArray alloc]init];
    self.RecurrenceEventForDaySP = [[NSMutableArray alloc]init];
    [self.navigationController setNavigationBarHidden: YES];
    
    NSLog(@"initializing");
}

- (void) viewDidAppear:(BOOL)animated {
    [self updateEventList];
    UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
    appWindow.rootViewController = self.navigationController;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)databaseUpdate:(NSInteger)command andResult:(NSInteger)result{
    if (result==0){
        [self.view makeToast:CONNECTION_CHECK duration:POPUP_DURATION position:POSITION_CENTER] ;
    }else{
        [self updateEventList];
    }
}
- (void)CreateSlot:(CGPoint)point initDate:(NSDate*) initDate offsetx:(NSInteger) x offsety:(NSInteger) y{
    NSLog(@"Successful");
    NSLog(@"%i %i",x,y);
    NSTimeInterval seconds =(x+1)* ONE_DAY_SECONDS + y* ONE_HOUR_SECONDS - 48*60*60;
    NSDate* newdate= [initDate dateByAddingTimeInterval: seconds];
    NSDate* newdate2= [initDate dateByAddingTimeInterval: seconds+ONE_HOUR_SECONDS];
    
    NSLog([newdate description]);
    NewEventController *newEventVC = [[NewEventController alloc] init];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm";
    newEventVC.startTiemPicker.date=newdate;
    newEventVC.saveDelegate = self;
    [newEventVC.startTimeInput setText:[dateFormatter stringFromDate:newdate]];
    newEventVC.endTimePicker.date=newdate2;
    [newEventVC.endTimeInput setText:[dateFormatter stringFromDate:newdate2]];
    
    newEventVC.view.frame = CGRectMake(NEW_EVENT_FRAME_X, NEW_EVENT_FRAME_Y, NEW_EVENT_FRAME_WIDTH, NEW_EVENT_FRAME_HEIGHT);
    TSPopoverController *popoverController = [[TSPopoverController alloc] initWithContentViewController:newEventVC];
    newEventVC.controller = popoverController;
    popoverController.cornerRadius = BUTTON_RADIUS;
    popoverController.titleText = @"Add Free Slot";
    popoverController.popoverBaseColor = [UIColor colorWithRed:80/255.0 green:194/255.0 blue:169/255.0 alpha:1];
    popoverController.popoverGradient= NO;
    //    popoverController.arrowPosition = TSPopoverArrowPositionHorizontal;
    [popoverController showPopoverWithRect:CGRectMake(POPOVER_FRAME_X, POPOVER_FRAME_Y, POPOVER_FRAME_WIDTH, POPOVER_FRAME_HEIGHT)];
    newEventVC.startTiemPicker.date=newdate;
    [newEventVC.startTimeInput setText:[dateFormatter stringFromDate:newdate]];
    newEventVC.endTimePicker.date=newdate2;
    [newEventVC.endTimeInput setText:[dateFormatter stringFromDate:newdate2]];
}

@end
