//
//  loadingDelegate.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-2.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#ifndef TalenoxScheduler_loadingDelegate_h
#define TalenoxScheduler_loadingDelegate_h

@protocol loadingDelegate <NSObject>

-(void) nextViewController;

@end

#endif
