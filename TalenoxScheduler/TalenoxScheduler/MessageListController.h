//
//  MessageListController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 9/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "Constants.h"
#import "FriendMessage.h"
#import "MessageListCell.h"
#import "MessagePersonalController.h"
#import "MessageCompanyController.h"
#import "UserInfoModel.h"
#import "UIView+Toast.h"

@interface MessageListController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSMutableArray* messageList;
@property (nonatomic) NSString * userName;
@property (nonatomic, strong) Firebase* rootFirebase;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* companyName;
@property (nonatomic, strong) NSMutableString* hostURL;
@property (nonatomic, strong) NSMutableArray* chat;
@property (nonatomic, strong) NSMutableArray* conversationList;
@property (nonatomic, strong) NSMutableArray* populatedList;

- (IBAction)backButton:(id)sender;
-(void) getMessageList;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
