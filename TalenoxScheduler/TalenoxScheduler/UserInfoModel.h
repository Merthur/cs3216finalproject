//
//  UserInfoModel.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-11.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpClient.h"
#import "Constants.h"
#import "loadingDelegate.h"

@interface UserInfoModel : NSObject

@property (weak, nonatomic) id <loadingDelegate> loadingDelegate;

+ (UserInfoModel*)sharedInstance;

- (void)userLogin:(NSDictionary *)json;
- (NSDictionary*)getUserInformation;
- (NSDictionary*)getCompanyInformation;
- (NSArray*) getEmployeesInformation;
- (NSArray*)getUserApps;
- (NSString*) getToken;
- (NSString*) getUserId;
- (NSString*) getUserEmployeeId;
- (NSString*) getUserEmployeeProfilePictureUrl;
- (NSString*) getCompanyLogoUrl;
- (void) clearData;

@end
