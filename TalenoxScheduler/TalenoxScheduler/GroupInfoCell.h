//
//  GroupInfoCell.h
//  TalenoxScheduler
//
//  Created by Wen Yiran on 15/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupInfoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *employeeNameLabel;
@end
