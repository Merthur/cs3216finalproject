//
//  CalendarTabBarController.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-3.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import "CalendarTabBarController.h"

@interface CalendarTabBarController ()

@end

@implementation CalendarTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self tabBar] setTintColor:
     [UIColor colorWithRed:TABBAR_TINTCOLOR_RED green:TABBAR_TINTCOLOR_GREEN blue:TABBAR_TINTCOLOR_BLUE alpha:TABBAR_TINTCOLOR_ALPHA]];
    
    [[self tabBar] setSelectedImageTintColor:
     [UIColor colorWithRed:TABBAR_SELECTIMG_TINTCOLOR_RED green:TABBAR_SELECTIMG_TINTCOLOR_GREEN blue:TABBAR_SELECTIMG_TINTCOLOR_BLUE alpha:TABBAR_SELECTIMG_TINTCOLOR_ALPHA]];
    self.tabBar.backgroundColor = [UIColor colorWithWhite:TABBAR_BACKGROUND_COLOR alpha:TABBAR_BACKGROUND_ALPHA];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
