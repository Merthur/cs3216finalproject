//
//  NewEventController.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-22.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import "NewEventController.h"

@interface NewEventController ()

@property (nonatomic) BOOL isEditingTimes;

@end

@implementation NewEventController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.isEditingTimes = NO;
    self.startTiemPicker.timeZone = [NSTimeZone localTimeZone];
    self.endTimePicker.timeZone = [NSTimeZone localTimeZone];
    self.Recurrence.delegate = self;
    self.Recurrence.dataSource = self;

    //self.saveDelegate = self;
    // Do any additional setup after loading the view from its nib.
    _RecurrenceOption = @[WEEK, WEEKDAY, DAY];

    
    [self.backButton.layer setCornerRadius:BUTTON_RADIUS];
    [self.backButton setClipsToBounds:YES];
    [self.saveButton.layer setCornerRadius:BUTTON_RADIUS];
    [self.saveButton setClipsToBounds:YES];


    [self.times setText:DEFAULT_TIME];
    [self.times setReturnKeyType:UIReturnKeyDone];
    [self.Recurrence setHidden:YES];
    [self.endTimePicker setHidden:YES];
    
    [self.startTimeInput addTarget:self
                            action:@selector(showStartTimePicker:)
                  forControlEvents:UIControlEventEditingDidBegin];
    [self.endTimeInput addTarget:self
                          action:@selector(showEndTimePicker:)
                forControlEvents:UIControlEventEditingDidBegin];
    [self.recurrenceInput addTarget:self
                             action:@selector(showRecurrencePicker:)
                   forControlEvents:UIControlEventEditingDidBegin];
    
    self.startTimeInput.delegate = self;
    self.endTimeInput.delegate = self;
    self.recurrenceInput.delegate = self;
    self.times.delegate = self;
    
    [self.startTiemPicker addTarget:self
                             action:@selector(startTimeChanged:)
                   forControlEvents:UIControlEventValueChanged];
    [self.endTimePicker addTarget:self
                           action:@selector(endTimeChanged:)
                 forControlEvents:UIControlEventValueChanged];
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *currentDateString = [dateFormatter stringFromDate:currentDate];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:currentDate];
    NSInteger minute = [components minute];
    
    currentDateString = [currentDateString substringToIndex:[currentDateString length]-CURRENT_STR_LENGTH];
    if (minute < MIN_LENGTH) {
        currentDateString = [NSString stringWithFormat:@"%@%@", currentDateString, ZERO_MIN_TIME];
    } else {
        currentDateString = [NSString stringWithFormat:@"%@%@", currentDateString, THIRTY_MIN_TIME];
    }
    [self.startTimeInput setText:currentDateString];
    
    [self.recurrenceInput setText:[self.RecurrenceOption objectAtIndex:[self.Recurrence selectedRowInComponent:0]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)cancelButtonPressed:(id)sender {
    [self.controller dismissPopoverAnimatd:YES];
}

- (IBAction)saveButtonPressed:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMATE;
    NSDate *start = [dateFormatter dateFromString:[self.startTimeInput text]];
    NSDate *end = [dateFormatter dateFromString:[self.endTimeInput text]];
    
    if([start compare:end] == NSOrderedDescending){
        [self.view makeToast:TIME_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
    }else if(![self isNumeric:[self.times text]]){
        [self.view makeToast:INPUT_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
    }
    else if (([[self.times text] intValue]>MAX_DAY)&&([self.Recurrence selectedRowInComponent:ZERO]==ONE)){
        [self.view makeToast:REPEAT_DAY_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        
    }else if (([[self.times text] intValue]>MAX_WEEK)&&([self.Recurrence selectedRowInComponent:ZERO]==ZERO)){
        [self.view makeToast:REPEAT_WEEK_CHECK duration:POPUP_DURATION position:POSITION_CENTER];
        
    }else{
        
        self.event = [[MAEvent alloc]init];
        
        self.event.backgroundColor = [UIColor colorWithRed:225/255.0 green:236/255.0 blue:187/255.0 alpha:0.6];
        self.event.textColor = [UIColor whiteColor];
        self.event.allDay = NO;
        self.event.userInfo = nil;
        self.event.title = @"";
        
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = DATE_FORMATE;
        self.event.start = [dateFormatter dateFromString:[self.startTimeInput text]];
        self.event.end = [dateFormatter dateFromString:[self.endTimeInput text]];
        
        self.event.recurrence = [self.Recurrence selectedRowInComponent:0];
        self.event.times=[[self.times text] intValue];
        [self.saveDelegate saveEvent:self.event];
        [self.controller dismissPopoverAnimatd:YES];
    }
    
}

-(void) showStartTimePicker:(id)sender {
    [self.startTimeInput resignFirstResponder];
    [self.startTiemPicker setHidden:NO];
    [self.endTimePicker setHidden:YES];
    [self.Recurrence setHidden:YES];
}

-(void) showEndTimePicker:(id)sender {
    [self.endTimeInput resignFirstResponder];
    [self.endTimePicker setHidden:NO];
    [self.startTiemPicker setHidden:YES];
    [self.Recurrence setHidden:YES];
}

-(void) showRecurrencePicker:(id)sender {
    [self.recurrenceInput resignFirstResponder];
    [self.startTiemPicker setHidden:YES];
    [self.endTimePicker setHidden:YES];
    [self.Recurrence setHidden:NO];
}

-(void) startTimeChanged:(id)sender {
    NSDate *startTime = [self.startTiemPicker date];
    NSDate *endTime = [self.endTimePicker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMATE;
    [self.startTimeInput setText:[dateFormatter stringFromDate:startTime]];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsForFirstDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:startTime];
    
    NSDateComponents *componentsForSecondDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:endTime];
    
    if ([componentsForFirstDate day] == [componentsForSecondDate day] && [componentsForFirstDate month] == [componentsForSecondDate month] &&[componentsForFirstDate year] == [componentsForSecondDate year] ){
        
    }
    else{
        self.endTimePicker.date = startTime;
        endTime = startTime;
        [self.endTimeInput setText:[dateFormatter stringFromDate:endTime]];
    }
}

-(void) endTimeChanged:(id)sender {
    NSDate *endTime = [self.endTimePicker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = DATE_FORMATE;
    
    [self.endTimeInput setText:[dateFormatter stringFromDate:endTime]];
}

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return ONE;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return _RecurrenceOption.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return _RecurrenceOption[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self.recurrenceInput setText:[self.RecurrenceOption objectAtIndex:row]];
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    if (![textField isEqual:self.times]) {
        [textField resignFirstResponder];
        self.isEditingTimes = NO;
    } else {
        self.isEditingTimes = YES;
    }
}

- (BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid=NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    return !self.isEditingTimes;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    self.isEditingTimes = NO;
    return YES;
}


@end
