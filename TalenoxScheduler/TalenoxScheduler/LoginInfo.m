//
//  LoginInfo.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-18.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import "LoginInfo.h"

@interface LoginInfo ()

@property (nonatomic) NSString *email;
@property (nonatomic) NSString *password;

@end


@implementation LoginInfo

- (LoginInfo*) initWithEmail:(NSString *)em andPassword:(NSString *)pwd {
    self = [super init];
    
    if (self) {
        self.email = em;
        self.password = pwd;
    }
    
    return self;
}

- (NSString*) getEmail {
    return self.email;
}

- (NSString*) getPassword {
    return self.password;
}

- (void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject: self.email forKey: KEY_EMAIL];
    [aCoder encodeObject: self.password forKey: KEY_PASSWORD];
}

- (LoginInfo *) initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    
    if (self) {
        self.email = [decoder decodeObjectForKey: KEY_EMAIL];
        self.password = [decoder decodeObjectForKey: KEY_PASSWORD];
    }
    
    return self;
}

@end
