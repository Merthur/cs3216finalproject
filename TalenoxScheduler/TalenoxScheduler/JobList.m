//
//  JobList.m
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-12.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import "JobList.h"

static BOOL loaded = NO;

@interface JobList ()

@property (nonatomic) NSMutableArray *jobList;
@property (nonatomic) NSMutableArray *appliedList;

@end


@implementation JobList

+ (JobList*)sharedInstance {
    static JobList *sharedInstance;
    
    if (!loaded) {
        sharedInstance = [[self alloc] init];
    }
    
    return sharedInstance;
}

- (JobList*) init {
    self = [super init];
    
    if (self) {
        [self clearData];
        [self getJobListFromServer];
    }
    
    return self;
}

- (void) withLoadingDelegate:(id<loadingDelegate>)delegate {
    if (loaded) {
        [delegate nextViewController];
    } else {
        self.loadingDelegate = delegate;
        loaded = YES;
    }
}

- (void) getJobListFromServer {
    [[HttpClient sharedInstance] getJobListWithEmployeeID:@""
                                                  success:^(NSURLSessionDataTask *task, id responseObject) {
                                                      [self createJobListWithResponse:responseObject];
                                                      [self getAppliedListFromServer];
                                                  }
                                                  failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                      NSLog(@"%@", error);
                                                      [self.loadingDelegate nextViewController];
                                                  }];
}

- (void) createJobListWithResponse: (id) response {
    NSArray *jobs = [response objectForKey:@"job_openings"];
    for (id jobItem in jobs) {
        Job *job = [[Job alloc] init];
        if (![[jobItem objectForKey:@"name"] isEqual: [NSNull null]]) {
            job.shiftName = [jobItem objectForKey:@"name"];
        }
        if (![[jobItem objectForKey:@"id"] isEqual: [NSNull null]]) {
            job.jobID = [[jobItem objectForKey:@"id"] integerValue];
        }
        if (![[jobItem objectForKey:@"min_salary"] isEqual: [NSNull null]]) {
            job.minSalary = [[jobItem objectForKey:@"min_salary"] doubleValue];
        }
        if (![[jobItem objectForKey:@"max_salary"] isEqual: [NSNull null]]) {
            job.maxSalary = [[jobItem objectForKey:@"max_salary"] doubleValue];
        }
        if (![[jobItem objectForKey:@"salary"] isEqual: [NSNull null]]) {
            job.salary = [[jobItem objectForKey:@"salary"] doubleValue];
        }
        if (![[jobItem objectForKey:@"salary_type"] isEqual: [NSNull null]]) {
            job.salaryType = [jobItem objectForKey:@"salary_type"];
        }
        if (![[jobItem objectForKey:@"description"] isEqual: [NSNull null]]) {
            job.jobDescription = [jobItem objectForKey:@"description"];
        }
        if (![[jobItem objectForKey:@"deadline"] isEqual: [NSNull null]]) {
            job.deadline = [jobItem objectForKey:@"deadline"];
        }
        if (![[jobItem objectForKey:@"work_start_time"] isEqual: [NSNull null]]) {
            job.startTime = [jobItem objectForKey:@"work_start_time"];
        }
        if (![[jobItem objectForKey:@"work_end_time"] isEqual: [NSNull null]]) {
            job.endTime = [jobItem objectForKey:@"work_end_time"];
        }
        if (![[jobItem objectForKey:@"location"] isEqual: [NSNull null]]) {
            job.location = [jobItem objectForKey:@"location"];
        }
        if (![[jobItem objectForKey:@"working_time_info"] isEqual: [NSNull null]]) {
            job.workingTimeInfo = [jobItem objectForKey:@"working_time_info"];
        }

        [self.jobList addObject:job];
    }
}

- (void) getAppliedListFromServer {
    [[HttpClient sharedInstance]
     getApplicationHistoryWithEmployeeID:@""
     success:^(NSURLSessionDataTask *task, id responseObject) {
         [self createAppliedListWithResponse:responseObject];
     }
     failure:^(NSURLSessionDataTask *task, NSError *error) {
         NSLog(@"%@", error);
     }];
}

- (void) createAppliedListWithResponse: (id) response {
    NSArray *applied = [response objectForKey:@"job_application_histories"];
    for (NSDictionary *jobItem in applied) {
        JobApplied *job = [[JobApplied alloc] init];
        if (![[jobItem objectForKey:@"id"] isEqual: [NSNull null]]) {
            job.appliedID = [[jobItem objectForKey:@"id"] integerValue];
        }
        if (![[jobItem objectForKey:@"job_opening_id"] isEqual: [NSNull null]]) {
            job.jobID = [[jobItem objectForKey:@"job_opening_id"] integerValue];
        }
        if (![[jobItem objectForKey:@"user_id"] isEqual: [NSNull null]]) {
            job.userID = [[jobItem objectForKey:@"user_id"] integerValue];
        }
        if (![[jobItem objectForKey:@"status"] isEqual: [NSNull null]]) {
            job.status = [[jobItem objectForKey:@"status"] integerValue];
        }
        
        [self.appliedList addObject:job];
    }
    [self.loadingDelegate nextViewController];
}

- (NSArray*) getJobList {
    return self.jobList;
}

- (NSArray*) getAppliedJobList {
    return self.appliedList;
}

- (void) appliedJob:(NSInteger)jobID withAppliedID:(NSInteger) appliedID {
    JobApplied *job = [[JobApplied alloc] init];
    job.jobID = jobID;
    job.appliedID = appliedID;
    job.userID = [[[UserInfoModel sharedInstance] getUserId] integerValue];
    [self.appliedList addObject:job];
}

- (void) withdrawJobWithAppliedID:(NSInteger) appliedID {
    int index = -1;
    for (int i=0; i<[self.appliedList count]; i++) {
        JobApplied *applied = [self.appliedList objectAtIndex:i];
        if (applied.appliedID == appliedID) {
            index = i;
            break;
        }
    }
    
    if (index > -1) {
        [self.appliedList removeObjectAtIndex:index];
    }
}

- (void) clearData {
    self.jobList = [[NSMutableArray alloc] init];
    self.appliedList = [[NSMutableArray alloc] init];
    loaded = NO;
}

@end
