//
//  Constants.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-11.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

//Message view
#define MINIMUM_ROWS        5
#define TABLE_CELL_MARGIN   40
#define TABLE_CELL_FONT     17
#define TABLE_CELL_WIDTH    212

//user information
#define FILE_LOGIN_INFO @"file_login_info"
#define KEY_USER        @"user"
#define KEY_USER_APP    @"apps"
#define KEY_TOKEN       @"token"
#define KEY_NAME        @"name"
#define KEY_COMPANY     @"company"
#define KEY_EMAIL       @"email"
#define KEY_PASSWORD    @"password"
#define KEY_LOGIN_INFO  @"login_info"
#define KEY_STATUS      @"status"
#define KEY_FIRST_NAME  @"first_name"
#define KEY_LAST_NAME   @"last_name"
#define KEY_PICTURE     @"profile_picture"
#define KEY_IMAGE       @"image"
#define KEY_IMAGE_FILE  @"image_file"
#define KEY_URL         @"url"

//applied job status
#define APPLY_PENDING   0
#define APPLY_REJECTED  1
#define APPLY_SUCCESS   2
#define STATUS_PENDING  @"Pending"
#define STATUS_REJECTED @"Rejected"
#define STATUS_SUCCESS  @"Success"

//Segues
#define MESSAGE_SEGUE               @"MessageControllerSegue"
#define DASHBOARD_CONTROLLER        @"dashboardViewController"
#define JOBLISTVIEW_CONTROLLER      @"jobListViewController"
#define COMPANY_PERSONAL_SEGUE      @"SegueFromCompanyToPersonalChat"
#define FRIENDLIST_COMPANY_SEGUE    @"SegueFromFriendListToCompanyChat"
#define FRIENDLIST_PERSONAL_SEGUE   @"SegueFromFriendListToPersonalChat"
#define GROUP_INFO_PERSONAL_SEGUE   @"SegueFromGroupInfoToPersonalChat"
#define LOGOUT_SEGUE                @"logoutSegue"
#define MESSAGE_BOX_SEGUE           @"MessageBox"
#define COMPANY_INFO_SEGUE          @"SegueFromCompanyToInfo"
#define APPLIED_TO_DETAIL_SEGUE     @"segueFromAppliedToDetail"
#define JOBLIST_DETIAL_SEGUE        @"SegueFromJobListToJobDetail"
#define EVENT_DETAIL_SEGUE          @"EVENT_DETAIL"

//Images
#define PERSONAL_CHAT_ICON          @"personal-chat-icon.png"
#define AVARTAR_ICON                @"avatar.png"

//firebase
#define kFirechatNS @"https://vivid-fire-1667.firebaseio.com/"

//color schemme
#define TABBAR_TINTCOLOR_RED 0
#define TABBAR_TINTCOLOR_GREEN 0
#define TABBAR_TINTCOLOR_BLUE 1
#define TABBAR_TINTCOLOR_ALPHA 1

#define TABBAR_SELECTIMG_TINTCOLOR_RED 1
#define TABBAR_SELECTIMG_TINTCOLOR_GREEN 1
#define TABBAR_SELECTIMG_TINTCOLOR_BLUE 1
#define TABBAR_SELECTIMG_TINTCOLOR_ALPHA 1

#define TABBAR_BACKGROUND_COLOR 0
#define TABBAR_BACKGROUND_ALPHA 0.3

//shape
#define BUTTON_RADIUS 5
#define EVENT_DETAIL_FRAME_X 0
#define EVENT_DETAIL_FRAME_Y 0
#define EVENT_DETAIL_FRAME_WIDTH 320
#define EVENT_DETAIL_FRAME_HEIGHT 350

#define POPOVER_FRAME_X 200
#define POPOVER_FRAME_Y 35
#define POPOVER_FRAME_WIDTH 20
#define POPOVER_FRAME_HEIGHT 20

#define NEW_EVENT_FRAME_X 0
#define NEW_EVENT_FRAME_Y 0
#define NEW_EVENT_FRAME_WIDTH 320
#define NEW_EVENT_FRAME_HEIGHT 400


//string
#define WEEK @"WEEK"
#define WEEKDAY @"WEEKDAY"
#define DAY @"Day"
#define DEFAULT_TIME @"1"

//condition
#define CURRENT_STR_LENGTH 2
#define MIN_LENGTH 30
#define ZERO_MIN_TIME @"00"
#define THIRTY_MIN_TIME @"30"
#define POPUP_DURATION 3
#define MAX_WEEK 52
#define MAX_DAY 365
#define ZERO 0
#define ONE 1
#define TWO 2
#define ONE_HOUR_SECONDS 3600
#define SEVEN_DAY_SECONDS 604800
#define ONE_DAY_SECONDS 86400

//position
#define POSITION_CENTER @"center"

//format
#define DATE_FORMATE @"yyyy-MM-dd HH:mm"
#define WEEKDAY_FORMATE @"EEEE"
#define SATURDAY @"Saturday"
#define SUNDAY @"Sunday"

//popup info
#define TIME_CHECK              @"Start time cannot be later than End time"
#define INPUT_CHECK             @"Input should be number only"
#define REPEAT_DAY_CHECK        @"Repeat time should be inside 1-365"
#define REPEAT_WEEK_CHECK       @"Repeat time should be inside 1-52"
#define CONNECTION_CHECK        @"Connection Problem: Cannot create this free slot"
#define CLASH_CHECK             @"There already exists an event in your chosen slot"
#define DELETED_CHECK           @"event has been deleted"
#define DELETED_FAILED_CHECK    @"delete failed"
#define DELETED_SUCCESS_CHECK   @"delete successfully"
#define UPDATE_SUCCESS          @"update successfully"
#define APPLY_JOB_SUCCESS       @"Applied Job Successfully"
#define APPLY_JOB_FAIL          @"Fail to apply. Try again later."
#define WITHDRAW_JOB_SUCCESS    @"Withdraw Job Successfully"
#define WITHDRAW_JOB_FAIL       @"Fail to withdraw. Try again later."