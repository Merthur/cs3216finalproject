//
//  MessagePersonalController.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-29.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "UIView+Toast.h"
#import "Constants.h"
#import "MessageCell.h"

@interface MessagePersonalController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString* nameSelf;
@property (nonatomic, strong) NSString* nameOther;
@property (nonatomic, strong) NSMutableString* hostURL;
@property (nonatomic, strong) NSMutableArray* chat;
@property (nonatomic, strong) Firebase* firebase;

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nameSelfLable;
@property (weak, nonatomic) IBOutlet UILabel *nameOtherLable;
- (IBAction)backPressed:(id)sender;

@end

