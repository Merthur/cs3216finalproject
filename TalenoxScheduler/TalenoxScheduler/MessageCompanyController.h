//
//  MessageController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 11/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import "UIView+Toast.h"
#import "Constants.h"
#import "MessageCell.h"
#import "TSPopoverController.h"
#import "MessagePersonalController.h"
#import "GroupInfoController.h"

@interface MessageCompanyController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* companyName;
@property (nonatomic, strong) NSMutableString* hostURL;
@property (nonatomic, strong) NSMutableArray* chat;
@property (nonatomic, strong) Firebase* firebase;

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nameField;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
- (IBAction)backPressed:(id)sender;

@end
