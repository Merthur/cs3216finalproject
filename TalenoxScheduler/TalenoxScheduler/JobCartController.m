//
//  MessageInboxController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "JobCartController.h"

@interface JobCartController ()

@property (nonatomic) Job *selectedJob;
@property (nonatomic) NSInteger selectedAppliedID;
@property (nonatomic) NSArray *appliedList;
@property (nonatomic) NSMutableArray *appliedJobs;
@property (nonatomic) NSMutableArray *appliedIDs;

@end


@implementation JobCartController

@synthesize tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void) nextViewController {
    self.selectedJob = [[Job alloc] init];
    self.appliedList = [[JobList sharedInstance] getAppliedJobList];
    self.appliedJobs = [[NSMutableArray alloc] init];
    self.appliedIDs = [[NSMutableArray alloc] init];
    
    [self addAppliedJobList];
    
    [self.view hideToastActivity];
}

- (void) addAppliedJobList {
    for (JobApplied *applied in self.appliedList) {
        for (Job *job in self.jobList) {
            if (job.jobID == applied.jobID) {
                [self.appliedJobs addObject:job];
                [self.appliedIDs addObject:[NSString stringWithFormat:@"%ld", (long)applied.appliedID]];
                break;
            }
        }
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [self.view makeToastActivity];
    self.selectedJob = [[Job alloc] init];
    [[JobList sharedInstance] withLoadingDelegate:self];
    self.appliedList = [[JobList sharedInstance] getAppliedJobList];
    self.appliedJobs = [[NSMutableArray alloc] init];
    self.appliedIDs = [[NSMutableArray alloc] init];
    
    [self addAppliedJobList];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    // We only have one section in our table view.
    return 1;
}

- (NSInteger)tableView:(UITableView*)table numberOfRowsInSection:(NSInteger)section
{
    // This is the number of chat messages.
    return [self.appliedJobs count];
}

// This method changes the height of the text boxes based on how much text there is.

- (UITableViewCell*)tableView:(UITableView*)table cellForRowAtIndexPath:(NSIndexPath *)index
{
    static NSString *simpleTableIdentifier = @"jobListCell";
    
    jobListCellTableViewCell *cell = (jobListCellTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"jobListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Job *curJob = [self.appliedJobs objectAtIndex:index.row];
    JobApplied *curAppliedJob = [self.appliedList objectAtIndex:index.row];
    cell.jobTitle.text = curJob.shiftName;
    cell.time.text = @"2014-11-4 13:00 - 18:00";
    //cell.location.text = curJob.location;
    cell.location.text = @"Location";
    
    switch (curAppliedJob.status) {
        case APPLY_PENDING:
            [cell.status setText:@"Pending"];
            //cell.status.backgroundColor = [UIColor colorWithRed:82/255.0 green:173/255.0 blue:216/255.0 alpha:1];
            cell.status.backgroundColor = [UIColor colorWithRed:113/255.0 green:236/255.0 blue:255/255.0 alpha:0.53];
            break;
            
        case APPLY_REJECTED:
            [cell.status setText:STATUS_REJECTED];
            cell.status.backgroundColor = [UIColor colorWithRed:179/255.0 green:204/255.0 blue:216/255.0 alpha:1];
            break;
            
        case APPLY_SUCCESS:
            [cell.status setText:STATUS_SUCCESS];
            cell.status.backgroundColor = [UIColor colorWithRed:238/255.0 green:133/255.0 blue:105/255.0 alpha:1];
            break;
            
        default:
            break;
    }
    cell.status.layer.cornerRadius = 3.0;
    cell.status.clipsToBounds = YES;
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedJob = [self.appliedJobs objectAtIndex:indexPath.row];
    self.selectedAppliedID = [[self.appliedIDs objectAtIndex:indexPath.row] integerValue];
    [self performSegueWithIdentifier:APPLIED_TO_DETAIL_SEGUE sender:self];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqual:APPLIED_TO_DETAIL_SEGUE]) {
        JobDetailController *jobDetailViewController = segue.destinationViewController;
        jobDetailViewController.startTime = self.selectedJob.startTime;
        jobDetailViewController.endTime = self.selectedJob.endTime;
        jobDetailViewController.location = self.selectedJob.location;
        jobDetailViewController.userName = [[[UserInfoModel sharedInstance]
                                             getUserInformation] objectForKey:KEY_NAME];
        jobDetailViewController.ownerName = [[[UserInfoModel sharedInstance]
                                              getCompanyInformation] objectForKey:KEY_NAME];
        jobDetailViewController.appliedID = self.selectedAppliedID;
        jobDetailViewController.shiftName = self.selectedJob.shiftName;
        jobDetailViewController.buttonMessage = @"Withdraw";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end


