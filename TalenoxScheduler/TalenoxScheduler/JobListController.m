//
//  JobListControllerController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "JobListController.h"
#import "jobListCellTableViewCell.h"
#import "JobDetailController.h"
#import "UserInfoModel.h"

@interface JobListController ()

@property (nonatomic) NSString *companyName;
@property (nonatomic) NSString *userName;
@property (nonatomic) NSArray * jobList;

@end

@implementation JobListController {
    Job *selectedJob;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view makeToastActivity];
    [[JobList sharedInstance] withLoadingDelegate:self];
}

-(void) nextViewController {
    self.companyName = [[[UserInfoModel sharedInstance] getCompanyInformation] objectForKey:KEY_NAME];
    self.userName = [[[UserInfoModel sharedInstance] getUserInformation] objectForKey:KEY_NAME];
    self.jobList = [[JobList sharedInstance] getJobList];
    
    self.jobListView.separatorColor = [UIColor whiteColor];
    self.jobListView.separatorInset = UIEdgeInsetsZero;
    [self.view hideToastActivity];
    [self.jobListView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.jobList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"jobListCell";
    
    jobListCellTableViewCell *cell = (jobListCellTableViewCell*)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"jobListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Job *curJob = (Job*)[self.jobList objectAtIndex:indexPath.row];
    cell.jobTitle.text = curJob.shiftName;
    cell.time.text = @"2014-11-4 13:00 - 18:00";
    //cell.location.text = curJob.location;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedJob = (Job*)[self.jobList objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:JOBLIST_DETIAL_SEGUE sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL) checkApplied {
    NSArray *appliedJobs = [[JobList sharedInstance] getAppliedJobList];
    for (JobApplied *item in appliedJobs) {
        if (item.jobID == selectedJob.jobID) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:JOBLIST_DETIAL_SEGUE]) {
        JobDetailController *jobDetailViewController = segue.destinationViewController;
        jobDetailViewController.startTime = selectedJob.startTime;
        jobDetailViewController.endTime = selectedJob.endTime;
        jobDetailViewController.location = selectedJob.location;
        jobDetailViewController.userName = self.userName;
        jobDetailViewController.ownerName = self.companyName;
        jobDetailViewController.jobID = selectedJob.jobID;
        jobDetailViewController.shiftName = selectedJob.shiftName;
        jobDetailViewController.jobDescription = selectedJob.jobDescription;
        if ([self checkApplied]) {
            jobDetailViewController.buttonMessage = @"Applied";
        } else {
            jobDetailViewController.buttonMessage = @"Apply";
        }
    }
}

@end
