//
//  PersonalProfile.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 15/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonalProfile : NSObject

@property (nonatomic) NSString * profilePicture;
@property (nonatomic) NSString * userName;

-(id) initWithPicture: (NSString*) picutre andName: (NSString*) name;

@end
