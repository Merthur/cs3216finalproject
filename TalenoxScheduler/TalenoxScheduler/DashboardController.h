//
//  DashboardController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 11/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfoModel.h"
#import "EventListModel.h"
#import "MessageListController.h"
#import "MessageCompanyController.h"
#import "JobListController.h"
#import "Constants.h"

@interface DashboardController : UIViewController

@property UserInfoModel * userInfo;
@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;

@end
