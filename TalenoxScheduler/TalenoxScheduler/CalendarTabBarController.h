//
//  CalendarTabBarController.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-3.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface CalendarTabBarController : UITabBarController

@end
