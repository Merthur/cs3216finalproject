//
//  UserAuthendicationController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 11/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpClient.h"
#import "UIView+Toast.h"
#import "UserInfoModel.h"
#import "DashboardController.h"
#import "LoginInfo.h"
#import "Constants.h"

@interface UserAuthendicationController : UIViewController<loadingDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *userPwd;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (nonatomic) UserInfoModel* userInfo;
@property (strong, nonatomic) IBOutlet UIImageView *BGimage;

- (IBAction)fetchUserInfo:(id)sender;


@end
