//
//  HttpConstant.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-11.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#ifndef TalenoxScheduler_HttpConstant_h
#define TalenoxScheduler_HttpConstant_h

//request serializer
#define ACCEPT              @"Accept"
#define ACCEPT_VALUE        @"*/*"
#define CONTENT_TYPE        @"Content-Type"
#define CONTENT_TYPE_VALUE  @"application/json"
#define HOST                @"host"
#define HOST_VALUE          @"www.talenox.com"
#define RUNSCOPE            @"Runscope-Request-Source"
#define RUNSCOPE_VALUE      @"dashboard"
#define USER_AGENT          @"User-Agent"
#define USER_AGENT_VALUE    @"runscope/1.0"

//data
#define EMAIL               @"email"
#define PASSWORD            @"password"
#define EMPLOYEE_ID         @"employee_id"
#define START_TIME          @"start_time"
#define END_TIME            @"end_time"
#define JOB_ID              @"job_opening_id"

//domain
#define REQUEST_DOMAIN          @"https://www.talenox.com/api/v1/"
#define LOGIN_REQUEST           @"auth/login"
#define GET_COMPANY_REQUEST     @"company"
#define GET_EMPLOYEE_REQUEST    @"employees"
#define CREATE_EVENT            @"rostering/availabilities"
#define GET_EMPLOYEE_FREE_SLOT  @"rostering/acalendar/employees/"
#define GET_JOB_LIST            @"job_openings"
#define GET_APPLICATION_HISTORY @"job_application_histories"
#define GET_EMPLOYEE_INIT       @"/init"

#endif
