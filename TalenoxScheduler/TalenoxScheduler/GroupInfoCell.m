//
//  GroupInfoCell.m
//  TalenoxScheduler
//
//  Created by Wen Yiran on 15/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "GroupInfoCell.h"

@implementation GroupInfoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
