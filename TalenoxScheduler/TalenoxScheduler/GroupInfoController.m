//
//  GroupInfoController.m
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 15/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import "GroupInfoController.h"

@interface GroupInfoController ()

@property (nonatomic) NSArray *employObjects;
@property (nonatomic) PersonalProfile *selected;

@end

@implementation GroupInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.groupList = [[NSMutableArray alloc] init];
    self.employObjects = [[UserInfoModel sharedInstance] getEmployeesInformation];
    [self createEmployeeModels];
    [self createCompanyLogo];
    self.groupNameLabel.text = self.groupName;
}

- (void) createCompanyLogo {
    BOOL useDefaultImage = NO;
    
    UIImage *image = [UIImage imageWithData:
                      [NSData dataWithContentsOfURL:
                       [NSURL URLWithString:
                        [[UserInfoModel sharedInstance] getCompanyLogoUrl]]]];
    
    if (!image) {
        image = [UIImage imageNamed: PERSONAL_CHAT_ICON];
        useDefaultImage = YES;
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(100, 100));
    [image drawInRect:CGRectMake(0,0,100,100)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.groupImageView setImage:newImage];
    if (!useDefaultImage) {
        [self.groupImageView setBackgroundColor:[UIColor whiteColor]];
    }
    
    self.groupImageView.layer.cornerRadius = 50.0;
    self.groupImageView.layer.masksToBounds = YES;
    self.groupImageView.layer.opacity = 0.6;
}

- (void) createEmployeeModels {
    for (id item in self.employObjects) {
        NSString *firstName = [item objectForKey: KEY_FIRST_NAME];
        firstName = [firstName stringByAppendingString:@" "];
        NSString *lastName = [item objectForKey: KEY_LAST_NAME];
        NSDictionary *imageObject = [[item objectForKey:KEY_PICTURE] objectForKey:KEY_IMAGE];
        NSString *imgUrl;
        if (![imageObject isEqual:[NSNull null]]) {
            imgUrl = [[[imageObject objectForKey: KEY_IMAGE_FILE] objectForKey:KEY_IMAGE_FILE] objectForKey: KEY_URL];
        } else {
            imgUrl = nil;
        }
        
        PersonalProfile *profile = [[PersonalProfile alloc]
                                    initWithPicture:imgUrl
                                    andName:[firstName stringByAppendingString:lastName]];
        
        [self.groupList addObject:profile];
    }
    [self.tableView reloadData];
}

- (IBAction)backButton:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.groupList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *groupInfoCellIdentifier = @"GroupInfoCell";
    
    GroupInfoCell *cell = (GroupInfoCell *)[tableView dequeueReusableCellWithIdentifier:groupInfoCellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:groupInfoCellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    PersonalProfile *cur = (PersonalProfile*)[self.groupList objectAtIndex:indexPath.row];
    
    UIImage *image;
    BOOL useDefaultImage = NO;
    if (cur.profilePicture == nil) {
        image = [UIImage imageNamed: AVARTAR_ICON];
        useDefaultImage = YES;
    } else {
        image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: cur.profilePicture]]];
    }
    UIGraphicsBeginImageContext(CGSizeMake(45, 45));
    [image drawInRect:CGRectMake(0,0,45,45)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [cell.profileImageView setImage:newImage];
    if (!useDefaultImage) {
        [cell.profileImageView setBackgroundColor:[UIColor whiteColor]];
    }
    cell.profileImageView.layer.cornerRadius = 22.5;
    cell.profileImageView.layer.masksToBounds = YES;
    cell.employeeNameLabel.text = cur.userName;
    cell.profileImageView.layer.opacity = 0.7;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selected = [self.groupList objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier: GROUP_INFO_PERSONAL_SEGUE sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:GROUP_INFO_PERSONAL_SEGUE]) {
        MessagePersonalController *personalVC = segue.destinationViewController;
        personalVC.nameSelf = [[[UserInfoModel sharedInstance] getUserInformation] objectForKey:KEY_NAME];
        personalVC.nameOther = self.selected.userName;
    }
}

@end
