//
//  AppDelegate.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-10-5.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

