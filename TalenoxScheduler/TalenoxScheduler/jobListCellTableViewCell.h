//
//  jobListCellTableViewCell.h
//  TalenoxScheduler
//
//  Created by Wen Yiran on 2/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface jobListCellTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *jobTitle;
@property (strong, nonatomic) IBOutlet UILabel *location;
@property (strong, nonatomic) IBOutlet UILabel *time;
@property (strong, nonatomic) IBOutlet UILabel *status;

@end
