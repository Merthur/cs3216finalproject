//
//  LoginInfo.h
//  TalenoxScheduler
//
//  Created by 张艺川 on 14-11-18.
//  Copyright (c) 2014年 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface LoginInfo : NSObject

- (LoginInfo*) initWithEmail:(NSString*)em andPassword:(NSString*)pwd;
- (NSString*) getEmail;
- (NSString*) getPassword;

@end
