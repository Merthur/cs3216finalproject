//
//  FriendMessage.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 10/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendMessage : NSObject

@property (nonatomic) NSString * friendName;
@property (nonatomic) NSString * messageTime;
@property (nonatomic) NSString * lastMessage;

-(id) init;
-(id) initWithFriendName: (NSString*) fName andMessageTime: (NSString *)mTime andLastMessage : (NSString*) message;

@end
