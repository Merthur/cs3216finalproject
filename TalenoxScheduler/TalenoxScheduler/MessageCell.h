//
//  MessageCell.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 22/10/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UITextView *messageView;

@end
