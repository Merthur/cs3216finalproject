//
//  GroupInfoController.h
//  TalenoxScheduler
//
//  Created by Kevin, Chen Hao on 15/11/14.
//  Copyright (c) 2014 NUS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PersonalProfile.h"
#import <UIKit/UIKit.h>
#import "GroupInfoCell.h"
#import "UserInfoModel.h"
#import "MessagePersonalController.h"

@interface GroupInfoController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSMutableArray * groupList;
@property (nonatomic) NSInteger * groupCount;
@property (nonatomic) NSString * groupName;
@property (strong, nonatomic) IBOutlet UIImageView *groupImageView;
@property (strong, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backButton:(id)sender;

@end
